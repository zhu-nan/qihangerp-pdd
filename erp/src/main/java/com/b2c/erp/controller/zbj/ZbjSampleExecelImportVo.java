package com.b2c.erp.controller.zbj;

public class ZbjSampleExecelImportVo {
    private String code;
    private Integer count;
//    private String img;
//    private String skuInfo;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

//    public String getImg() {
//        return img;
//    }
//
//    public void setImg(String img) {
//        this.img = img;
//    }
//
//    public String getSkuInfo() {
//        return skuInfo;
//    }
//
//    public void setSkuInfo(String skuInfo) {
//        this.skuInfo = skuInfo;
//    }
}
