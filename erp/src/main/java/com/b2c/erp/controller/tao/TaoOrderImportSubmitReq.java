package com.b2c.erp.controller.tao;

import com.b2c.entity.vo.OrderImportPiPiEntity;

import java.util.List;

public class TaoOrderImportSubmitReq {

    private Integer buyerUserId;//购买客户id
    private Integer shopId;

    private List<OrderImportPiPiEntity> orderList;//导入的订单

    
    public Integer getBuyerUserId() {
        return buyerUserId;
    }

    public void setBuyerUserId(Integer buyerUserId) {
        this.buyerUserId = buyerUserId;
    }

    public List<OrderImportPiPiEntity> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<OrderImportPiPiEntity> orderList) {
        this.orderList = orderList;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }
}
