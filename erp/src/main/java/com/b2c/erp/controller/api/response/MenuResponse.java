package com.b2c.erp.controller.api.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class MenuResponse {
    private String label = "";
    private String path="";
    private String icon ="";
    private String meta="";
    private Integer parentId=0;
//    [{
//        label: "首页",
//                path: "/wel/index",
//                icon: 'el-icon-document',
//                meta: {
//            i18n: 'dashboard',
//        },
//        parentId: 0
//    },
//    {
//        label: "测试",
//                icon: 'el-icon-document',
//            path: "/test",
//            meta: {
//        i18n: 'test',
//    },
//        parentId: 1
//    },
//    {
//        label: "更多",
//                icon: 'el-icon-document',
//            path: "/wel/more",
//            meta: {
//        menu: false,
//                i18n: 'more',
//    },
//        parentId: 2
//    }]
}
