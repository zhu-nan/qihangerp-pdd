package com.b2c.erp.controller.api;

import com.b2c.entity.api.ApiResult;
import com.b2c.entity.api.ApiResultEnum;
import com.b2c.erp.controller.api.response.LoginResp;
import com.b2c.erp.controller.api.response.MenuResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/v1")
public class MenuApiController {
    //    [{
//        label: "首页",
//                path: "/wel/index",
//                icon: 'el-icon-document',
//                meta: {
//            i18n: 'dashboard',
//        },
//        parentId: 0
//    },
//    {
//        label: "测试",
//                icon: 'el-icon-document',
//            path: "/test",
//            meta: {
//        i18n: 'test',
//    },
//        parentId: 1
//    },
//    {
//        label: "更多",
//                icon: 'el-icon-document',
//            path: "/wel/more",
//            meta: {
//        menu: false,
//                i18n: 'more',
//    },
//        parentId: 2
//    }]
    @RequestMapping("/user/getTopMenu")
    public ApiResult<List<MenuResponse>> get(){
        List<MenuResponse> menus =new ArrayList<>();
        MenuResponse e = new MenuResponse("首页","/wel/index","el-icon-document","",0);
        menus.add(e);
        e = new MenuResponse("测试","/test","el-icon-document","",1);
        menus.add(e);
//        e = new MenuResponse("更多","/wel/more","el-icon-document","",2);
//        menus.add(e);
        return new ApiResult<>(ApiResultEnum.SUCCESS,menus);
    }
    @RequestMapping("/user/getMenu")
    public ApiResult<List<MenuResponse>> getMenu(){
        List<MenuResponse> menus =new ArrayList<>();
        MenuResponse e = new MenuResponse("首页","/wel/index","el-icon-document","",0);
        menus.add(e);
        e = new MenuResponse("测试","/test","el-icon-document","",1);
        menus.add(e);
        e = new MenuResponse("更多","/wel/more","el-icon-document","",2);
        menus.add(e);
        return new ApiResult<>(ApiResultEnum.SUCCESS,menus);
    }
}
