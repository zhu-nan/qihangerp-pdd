package com.b2c.erp.youzan;

import com.alibaba.fastjson.JSONArray;

import java.math.BigDecimal;

/**
 * @Description: pbd add 2019/6/25 21:05
 */
public class SkuStockVo {
    private BigDecimal price;
    private Long quantity;
    private String item_no;
    private JSONArray skus;

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public String getItem_no() {
        return item_no;
    }

    public void setItem_no(String item_no) {
        this.item_no = item_no;
    }

    public JSONArray getSkus() {
        return skus;
    }

    public void setSkus(JSONArray skus) {
        this.skus = skus;
    }
}
