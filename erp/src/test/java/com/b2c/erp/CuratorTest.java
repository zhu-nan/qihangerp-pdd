//package com.b2c.erp;
//
//import org.apache.curator.RetryPolicy;
//import org.apache.curator.framework.CuratorFramework;
//import org.apache.curator.framework.CuratorFrameworkFactory;
//import org.apache.curator.framework.api.BackgroundCallback;
//import org.apache.curator.framework.api.CuratorEvent;
//import org.apache.curator.retry.ExponentialBackoffRetry;
//import org.apache.zookeeper.CreateMode;
//import org.apache.zookeeper.data.Stat;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//
//import java.nio.charset.StandardCharsets;
//import java.util.List;
//
//public class CuratorTest {
//    private CuratorFramework client;
//    @Before
//    public void testConnect(){
//        RetryPolicy retryPolicy = new ExponentialBackoffRetry(3000,10);
////        CuratorFramework client = CuratorFrameworkFactory.newClient("192.168.253.130:2181",retryPolicy);
////        client.start();
//
//        //连式编程
//        client = CuratorFrameworkFactory.builder()
//                .connectString("192.168.253.130:2181")
//                .sessionTimeoutMs(60*1000)
//                .connectionTimeoutMs(15 * 1000)
//                .retryPolicy(retryPolicy)
//                .namespace("zkTest")
//                .build();
//        client.start();
//    }
//
//    /**
//     * 创建节点 create 持久 临时 顺序 数据操作
//     * 1、基本创建
//     * 2、创建节点 带有数据
//     * 3、设置节点类型
//     * 4、创建多级节点
//     */
//    @Test
//    public void testCreate() throws Exception {
//        String path = client.create().forPath("/app1");
//        System.out.println(path);
//    }
//
//    /**
//     * 带有数据
//     * @throws Exception
//     */
//    @Test
//    public void testCreate2() throws Exception {
//        String path = client.create().forPath("/app2","Hello world".getBytes(StandardCharsets.UTF_8));
//        System.out.println(path);
//    }
//    @Test
//    public void testCreate3() throws Exception {
//        String path = client.create().withMode(CreateMode.EPHEMERAL).forPath("/app3");
//        System.out.println(path);
//    }
//
//    @Test
//    public void testCreate4() throws Exception {
//        //创建多级节点
//        String path = client.create().creatingParentsIfNeeded().forPath("/app4/p1");
//        System.out.println(path);
//    }
//
//    /**
//     * 查询节点
//     * 1、查询数据 get
//     * 2、查询子节点 ls
//     * 3、查询节点状态信息：ls -s
//     * @throws Exception
//     */
//    @Test
//    public void testGet1() throws Exception {
//        //创建多级节点
//        byte[] data = client.getData().forPath("/app2");
//        System.out.println(new String(data));
//    }
//
//    @Test
//    public void testGet2() throws Exception {
//        //查询多级节点
//        List<String> path = client.getChildren().forPath("/app4");
//        System.out.println(path);
//    }
//    @Test
//    public void testGet3() throws Exception {
//        //创建多级节点
//        Stat stat = new Stat();
//        client.getData().storingStatIn(stat).forPath("/app4");
//        System.out.println(stat);
//    }
//
//    /**
//     * 修改数据
//     * 1、修改数据
//     * 2、根据版本修改数据
//     * @throws Exception
//     */
//    @Test
//    public void testSet() throws Exception {
//        //修改数据
//        client.setData().forPath("/app1","TestSET".getBytes(StandardCharsets.UTF_8));
//    }
//
//    @Test
//    public void testSetForVersion() throws Exception {
//        //根据版本修改数据
//        Stat stat = new Stat();
//        //查询节点信息
//        client.getData().storingStatIn(stat).forPath("/app1");
//        int version = stat.getVersion();
//        client.setData().withVersion(version).forPath("/app1","TestSET".getBytes(StandardCharsets.UTF_8));
//    }
//
//    /**
//     * 删除节点
//     * 1、删除节点
//     * 2、删除带有子节点的节点
//     * 3、必须成功的删除
//     * 4、回调
//     * @throws Exception
//     */
//    @Test
//    public void testDel() throws Exception {
//        //删除节点
//        client.delete().forPath("/app2");
//    }
//
//    @Test
//    public void testDel2() throws Exception {
//        //删除带有子节点的节点
//        client.delete().deletingChildrenIfNeeded().forPath("/app2");
//    }
//    @Test
//    public void testDel3() throws Exception {
//        //必须成功的删除
//        client.delete().guaranteed().forPath("/app2");
//    }
//    @Test
//    public void testDel4() throws Exception {
//        //回调
//        client.delete().guaranteed().inBackground(new BackgroundCallback() {
//            @Override
//            public void processResult(CuratorFramework curatorFramework, CuratorEvent curatorEvent) throws Exception {
//
//            }
//        }).forPath("/app2");
//    }
//
//    @After
//    public void closeConnect(){
//        if(client!=null){
//            client.close();
//        }
//    }
//}
