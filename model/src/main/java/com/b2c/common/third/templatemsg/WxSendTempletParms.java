package com.b2c.common.third.templatemsg;

/**
 * @Description: pbd add 2019/4/2 10:05
 */
public class WxSendTempletParms {
    /**
     * 用户open_id
     */
    public String openId;
    /**
     * 模板id
     */
    public String temletId;
    /**
     * 模板跳转链接
     */
    public String url;
    /**
     * 值1
     */
    public String param_1;
    /**
     * 值2
     */
    public String param_2;

}
