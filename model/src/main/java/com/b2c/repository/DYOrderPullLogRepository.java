package com.b2c.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * 抖店订单更新日志
 */
@Repository
public class DYOrderPullLogRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * 插入日志
     * @param pullType 更新类型，收到SD，自动ZD
     * @param orderType 订单类型1订单2退货
     * @param dataCount 更新的数据量
     * @param dataStartTime 数据开始时间
     * @param dataEndTime 数据结束时间
     * @param dataResult 数据结果
     */
    public void insertLog(String pullType,Integer orderType,Integer dataCount,String dataStartTime,String dataEndTime,String dataResult){
        String sql="INSERT INTO dc_douyin_order_pull_log (pullType,orderType,dataCount,dataStartTime,dataEndTime,dataResult) VALUE (?,?,?,?,?,?) ";
        jdbcTemplate.update(sql,pullType,orderType,dataCount,dataStartTime,dataEndTime,dataResult);
    }
}
