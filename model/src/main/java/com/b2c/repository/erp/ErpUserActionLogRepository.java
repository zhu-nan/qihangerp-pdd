package com.b2c.repository.erp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.UUID;

import com.b2c.entity.enums.EnumUserActionType;

/**
 * 描述：
 * 仓库系统用户操作日志
 *
 * @author qlp
 * @date 2019-10-24 09:03
 */
@Repository
public class ErpUserActionLogRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * 添加用户操作日志
     * @param userId
     * @param actionType
     * @param actionUrl
     * @param actionIp
     * @param actionContent
     * @param actionResult
     */
    public void addUserAction(Integer userId, EnumUserActionType actionType,String actionUrl,String actionIp,String actionContent,String actionResult){
        try {
            String sql = "INSERT INTO erp_user_action_log (id,user_id,action_type,action_url,action_ip,action_content,action_result,action_time) VALUE (?,?,?,?,?,?,?,?)";
            UUID uuid = UUID.randomUUID();
            jdbcTemplate.update(sql, uuid.toString(), userId, actionType.getIndex(), actionUrl, actionIp, actionContent, actionResult, System.currentTimeMillis() / 1000);
        }catch (Exception e){

        }
    }
}
