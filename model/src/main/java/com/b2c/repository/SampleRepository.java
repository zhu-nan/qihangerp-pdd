package com.b2c.repository;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.SampleListEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Repository
public class SampleRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    protected int getTotalSize() {
        return jdbcTemplate.queryForObject("SELECT FOUND_ROWS() as row_num;", int.class);
    }

    /**
     * 获取直播间样品列表
     * @param pageIndex
     * @param pageSize
     * @param goodsNum
     * @param zbjId
     * @return
     */
    public PagingResponse<SampleListEntity> getSampleList(Integer pageIndex, Integer pageSize, String goodsNum, String orderDate, Integer zbjId) {
        StringBuffer sb = new StringBuffer();
        sb.append("SELECT SQL_CALC_FOUND_ROWS ");
        sb.append(" oi.id as itemId,oi.goodsId,oi.goodsNumber,oi.goodsTitle,oi.goodsImage,oi.skuInfo,oi.specId,oi.specNumber,oi.quantity,oi.itemStatus, ");
        sb.append("o.orderNum,o.buyerUserId,o.buyerName,o.sellerMemo,o.contactPerson,o.saleType,o.`status`,o.orderTime,o.deliveredStatus,o.auditStatus,o.orderDate,oi.orderId,");
        sb.append("g.remark,g.`length`,g.height,g.width,g.width1,g.width2,g.width3,g.weight,g.attr1,g.attr2,g.attr3,g.attr4,g.attr5,g.status as goodsStatus,");
        sb.append("IFNULL(g.cost_price,0) AS costPrice,IFNULL(g.salePrice,0) AS salePrice, ");
        sb.append("(SELECT SUM(currentQty) FROM erp_goods_stock_info esi WHERE esi.specId = oi.specId AND isDelete=0) AS currentQty  ");
        sb.append(" FROM erp_sales_order_item oi ");
        sb.append(" LEFT JOIN erp_sales_order o ON o.id = oi.orderId ");
        sb.append(" LEFT JOIN erp_goods g ON g.id = oi.goodsId ");
        sb.append(" WHERE 1=1  ");

        List<Object> params = new ArrayList<>();
        if (!StringUtils.isEmpty(goodsNum)) {
            sb.append(" AND oi.specNumber LIKE  ? ");
            params.add("%"+goodsNum+"%");
        }
        if (!StringUtils.isEmpty(orderDate)) {
            sb.append(" AND o.orderDate = ? ");
            params.add(orderDate);
        }
        if (!StringUtils.isEmpty(zbjId)) {
            sb.append("AND o.buyerUserId = ? ");
            params.add(zbjId);
        }

        sb.append(" ORDER BY o.orderDate DESC");
        if(pageSize != null){
            sb.append(" LIMIT ?,? ");
            params.add((pageIndex - 1) * pageSize);
            params.add(pageSize);
        }



        List<SampleListEntity> lists = jdbcTemplate.query(sb.toString(), new BeanPropertyRowMapper<>(SampleListEntity.class), params.toArray(new Object[params.size()]));
        if(pageSize == null){
            pageSize = lists.size();
        }
        return new PagingResponse<>(pageIndex, pageSize, getTotalSize(), lists);
    }
}
