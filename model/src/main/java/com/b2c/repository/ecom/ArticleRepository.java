package com.b2c.repository.ecom;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ecom.ArticleEntity;

@Repository
public class ArticleRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Transactional
    public PagingResponse<ArticleEntity> getList(Integer pageIndex, Integer pageSize, String title,
            String platform) {
                StringBuilder sb = new StringBuilder();
                sb.append("SELECT SQL_CALC_FOUND_ROWS kw.*");
               
                sb.append(" FROM ecom_article as kw ");
                sb.append(" where kw.isDelete=0 ");
        
                List<Object> params = new ArrayList<>();
                if (StringUtils.isEmpty(title) == false) {
                    sb.append(" AND kw.title LIKE ? ");
                    params.add("%"+title+"%");
                  
                }
                if (StringUtils.isEmpty(platform) == false) {
                    sb.append(" AND kw.platform = ?");
                    params.add(platform);
                }
               
                
        
                sb.append(" ORDER BY kw.id DESC LIMIT ?,? ");
                params.add((pageIndex - 1) * pageSize);
                params.add(pageSize);
        
                List<ArticleEntity> list = jdbcTemplate.query(sb.toString(), new BeanPropertyRowMapper<>(ArticleEntity.class), params.toArray(new Object[params.size()]));
                Integer totalSize = getTotalSize();
                return new PagingResponse<>(pageIndex, pageSize, totalSize, list);
    }

    protected int getTotalSize() {
        return jdbcTemplate.queryForObject("SELECT FOUND_ROWS() as row_num;", int.class);
    }

    public void add(String title,String content,String platform,String tag) {
        String sql ="INSERT INTO ecom_article (title,platform,content,tag) VALUE (?,?,?,?)";
        jdbcTemplate.update(sql, title,platform,content,tag);
    }
}
