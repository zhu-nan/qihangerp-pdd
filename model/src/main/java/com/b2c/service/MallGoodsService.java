// package com.b2c.service;

// import com.alibaba.fastjson.JSONArray;
// import com.b2c.entity.result.PagingResponse;
// import com.b2c.mall.entity.MallGoodsEntity;
// import com.b2c.mall.entity.MallGoodsVo;
// import com.b2c.entity.vo.GoodsAddVo;

// public interface MallGoodsService {
//      PagingResponse<MallGoodsEntity> getGoodsList(Integer pageIndex, Integer pageSize, String title, String number);
//     /**
//      * 查询商品信息
//      * @param id
//      * @return
//      */
//      MallGoodsVo getMallGoods(Long id);
//     /**
//      * 发布到云购
//      * @param addVo
//      * @param createBy
//      * @return
//      */
//      Integer addGoods(GoodsAddVo addVo, String createBy);
//     /**
//      * 添加商品
//      * @param array
//      */
//    void addMallGoods(JSONArray array);
// }
