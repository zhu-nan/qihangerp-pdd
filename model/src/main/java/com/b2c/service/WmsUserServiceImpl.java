package com.b2c.service;

import com.b2c.entity.result.PagingResponse;
import com.b2c.common.utils.MD5Utils;
import com.b2c.common.utils.RandomUtils;
import com.b2c.entity.enums.UserStateEnums;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.ManageGroupEntity;
import com.b2c.entity.WmsManagePermissionEntity;
import com.b2c.interfaces.WmsUserService;
import com.b2c.repository.ErpUserRepository;
import com.b2c.entity.vo.WmsManageUserMenuVo;
import com.b2c.entity.ManageUserEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description: pbd add 2019/9/28 10:02
 */
@Service
public class WmsUserServiceImpl implements WmsUserService {
    @Autowired
    private ErpUserRepository userRepository;

    @Override
    public PagingResponse<ManageUserEntity> getWmsManageUsers(int pageIndex, int pageSize, String name, String mobile) {
        return userRepository.getWmsManageUsers(pageIndex,pageSize,name,mobile);
    }

    @Override
    public ManageUserEntity getWmsManageUser(Integer userId) {
        return userRepository.getWmsManageUser(userId);
    }

    @Override
    public List<ManageGroupEntity> getManageGroups() {
        return userRepository.getManageGroups();
    }

    @Override
    public ResultVo<Integer> executeWmsManageUser(Integer userId,String userName, String name, String mobile, String pwd, String groupId, String state) {
        int mobileCount = userRepository.checkManageUserByMobile(userId, mobile);
        if (mobileCount > 0) return new ResultVo<>(EnumResultVo.Exist, "手机号已存在");
        userRepository.executeWmsManageUser(userId,userName, name, mobile, pwd, groupId, state);
        return new ResultVo<>(EnumResultVo.SUCCESS);
    }

    @Override
    public void delWmsManageUser(Integer userId) {
        userRepository.delWmsManageUser(userId);
    }
    @Override
    public ResultVo<ManageUserEntity> userLogin(String userName, String userPwd) {
        ManageUserEntity entity = userRepository.getUserByUserName(userName);
        if (entity == null) return new ResultVo(EnumResultVo.NotFound, "用户不存在");
        if(entity.getStatus()==UserStateEnums.LOCK.getIndex())return new ResultVo(EnumResultVo.NotFound, "用户已锁定,无法登录");
        String pwd = MD5Utils.MD5Encode(userPwd);
        String finalPwd = MD5Utils.MD5Encode(pwd + entity.getUserSalf());
        if (finalPwd.equals(entity.getUserPwd()) == false) return new ResultVo(EnumResultVo.DataError, "登录密码不正确");
        return new ResultVo<ManageUserEntity>(EnumResultVo.SUCCESS, entity);
    }

    @Override
    public boolean checkUserPermissionMenu(Integer userId, String permissionKey) {
        return userRepository.checkUserPermissionMenu(userId,permissionKey);
    }

    @Override
    public boolean checkUserPermissionMenuByUrl(Integer userId, String url) {
        return userRepository.checkUserPermissionMenuByUrl(userId,url);
    }

    @Override
    public WmsManagePermissionEntity getEntityByKey(String permissionKey) {
        return userRepository.getEntityByKey(permissionKey);
    }

    @Override
    public void initUserPermissionMenu(Integer userId) {
        userRepository.initUserPermissionMenu(userId);
    }

    @Override
    public void setManageUserMenu(Integer userId, String[] menuIds) {
        userRepository.setManageUserMenu(userId,menuIds);
    }

    @Override
    public List<WmsManageUserMenuVo> getUserPerMissionMenuByWMS(Integer userId, Integer type) {
        return userRepository.getUserPerMissionMenuByWMS(userId,type);
    }

    @Override
    public ResultVo<ManageUserEntity> updWmsUserPwd(int userId, String oldPwd, String newPwd) {
        ManageUserEntity user = userRepository.getWmsManageUser(userId);
        String finalPwd = MD5Utils.MD5Encode(MD5Utils.MD5Encode(oldPwd) + user.getUserSalf());
        if (finalPwd.equals(user.getUserPwd()) == false) return new ResultVo(EnumResultVo.DataError, "旧密码不正确");
        String pwd_rand = RandomUtils.randomString(8);
        userRepository.updWmsUserPwd(userId, MD5Utils.MD5Encode(MD5Utils.MD5Encode(newPwd) + pwd_rand), pwd_rand);
        return new ResultVo<>(EnumResultVo.SUCCESS, user);
    }
}
