package com.b2c.service;

import com.b2c.repository.GoodsCategoryAnalyseRepository;
import com.b2c.entity.vo.GoodsCategoryAnalyseVo;
import com.b2c.interfaces.GoodsCategoryAnalyseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodsCategoryAnalyseServiceImpl implements GoodsCategoryAnalyseService {
    @Autowired
    private GoodsCategoryAnalyseRepository repository;
    @Override
    public List<GoodsCategoryAnalyseVo> getAnalyseReport() {
        return repository.getAnalyseReport();
    }
}
