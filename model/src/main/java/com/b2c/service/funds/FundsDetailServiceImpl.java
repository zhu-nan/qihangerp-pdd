package com.b2c.service.funds;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.FundsDetailEntity;
import com.b2c.entity.funds.LedgerDayModel;
import com.b2c.entity.result.ResultVo;
import com.b2c.repository.FundsDetailRepository;
import com.b2c.interfaces.funds.FundsDetailService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FundsDetailServiceImpl  implements FundsDetailService {
    @Autowired
    private FundsDetailRepository repository;
    @Override
    public PagingResponse<FundsDetailEntity> getFundsDetailList(Integer pageIndex, Integer pageSize, Integer type, String source, String sourceNo, String createDate) {
        return repository.getFundsDetailList(pageIndex, pageSize, type, source, sourceNo, createDate);
    }
    @Override
    public ResultVo<Long> create(FundsDetailEntity fEntity) {
        
        return repository.create(fEntity);
    }
    @Override
    public List<LedgerDayModel> getLedgerDayReport(String startDate, String endDate) {
        
        return repository.getLedgerDayReport(startDate, endDate);
    }
}
