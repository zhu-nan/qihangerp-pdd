package com.b2c.service.funds;

import java.util.List;

import javax.annotation.Resource;

import com.b2c.interfaces.funds.OrderSettlementPddService;
import org.springframework.stereotype.Service;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.funds.OrderSettlementDetailPddVo;
import com.b2c.entity.funds.OrderSettlementPddEntity;
import com.b2c.entity.result.ResultVo;
import com.b2c.repository.funds.OrderSettlementPddRepository;

@Service
public class OrderSettlementPddServiceImpl implements OrderSettlementPddService {
    @Resource
    private OrderSettlementPddRepository repository;
  

    @Override
    public ResultVo<String> importExcelSettlementList(List<OrderSettlementPddEntity> list, Integer shopId) {
       
        return repository.importExcelSettlementList(list, shopId);
    }


    @Override
    public PagingResponse<OrderSettlementPddEntity> getList(Integer pageIndex, Integer pageSize, String orderSn,
            String startTime, String endTime) {
 
        return repository.getList(pageIndex, pageSize, orderSn, startTime, endTime);
    }


    @Override
    public PagingResponse<OrderSettlementDetailPddVo> getOrderSettlementList(Integer pageIndex, Integer pageSize,
            String orderSn, String startTime, String endTime) {

        return repository.getOrderSettlementList(pageIndex, pageSize, orderSn, startTime, endTime);
    }


    @Override
    public List<OrderSettlementDetailPddVo> getOrderSettlementList(Integer shopId, String startTime, String endTime) {

        return repository.getOrderSettlementList(shopId, startTime, endTime);
    }
    
}
