package com.b2c.service.wms;

import com.b2c.entity.result.PagingResponse;
import com.b2c.common.utils.DateUtil;
import com.b2c.entity.ErpGoodsStockInfoEntity;
import com.b2c.entity.ErpGoodsStockLossFormEntity;
import com.b2c.entity.erp.vo.ErpGoodsBadStockLogVo;
import com.b2c.entity.erp.vo.ErpGoodsBadStockVo;
import com.b2c.entity.erp.vo.ErpGoodsStockLossFormVo;
import com.b2c.repository.erp.StockBadRepository;
import com.b2c.interfaces.wms.StockBadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class StockBadServiceImpl implements StockBadService {
    @Autowired
    private StockBadRepository repository;

    @Override
    public PagingResponse<ErpGoodsBadStockVo> getStockBadList(Integer pageIndex, Integer pageSize, String number,Integer type) {
        return repository.getStockBadList(pageIndex,pageSize,number,type);
    }

    @Override
    public ErpGoodsBadStockVo getStockBad(Integer stockId) {
        return repository.getStockBad(stockId);
    }

    @Override
    public List<ErpGoodsBadStockLogVo> getBadLog(Integer stockId) {
        return repository.getBadLog(stockId);
    }

    @Override
    public List<ErpGoodsBadStockLogVo> getLIKEBad(String number) {
        return repository.getLIKEBad(number);
    }

    @Override
    public void lossAdd(Integer userId, String userName, String lossText, String billNo, String billDate, String[] specNumbers,  String[] quantities, String[] locationNames,String[] remark) {
        repository.lossAdd(userId,userName,lossText,billNo,billDate,specNumbers,quantities,locationNames,remark);
    }

    @Override
    public ErpGoodsStockInfoEntity getLocationAndQuanty(String LocationName, String specNumber) {
        return repository.getLocationAndQuanty(LocationName,specNumber);
    }


    @Override
    public PagingResponse<ErpGoodsStockLossFormEntity> getLossFromList(Integer pageIndex, Integer pageSize, String number, String startTime, String endTime) {
        Long start = 0L;
        if (!StringUtils.isEmpty(startTime)) start = DateUtil.strToLong(startTime);
        Long end = 0L;
        if (!StringUtils.isEmpty(startTime)) end = DateUtil.strToLong(endTime);
        return repository.getLossFromList(pageIndex, pageSize, number, start, end);
    }

    @Override
    public ErpGoodsStockLossFormVo getLossItem(Integer formId) {
        return repository.getLossItem(formId);
    }

    @Override
    public Integer checkLoss(Integer id, Integer userId, String userName) {
        return repository.checkLoss(id,userId,userName);
    }
}
