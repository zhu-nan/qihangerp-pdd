package com.b2c.service.wms;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.erp.ErpGoodsSpecEntity;
import com.b2c.entity.ErpGoodsSpecMoveFromEntiy;
import com.b2c.entity.erp.vo.*;
import com.b2c.entity.result.EnumResultVo;
import com.b2c.entity.result.ResultVo;
import com.b2c.repository.erp.StockRepository;
import com.b2c.repository.erp.ErpStockLocationRepository;
import com.b2c.interfaces.wms.StockService;
import com.b2c.entity.vo.ErpGoodsStockLogsListVo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-03-21 13:53
 */
@Service
public class StockServiceImpl implements StockService {
    @Autowired
    private StockRepository stockRepository;
    @Autowired
    private ErpStockLocationRepository erpStockLocationRepository;

    @Override
    public List<GoodsCategoryVo> getCategoryList() {
        return stockRepository.getCategoryList();
    }

    @Override
    public void addStockCategory(String name, int parentId, int id) {
        stockRepository.addStockCategory(name, parentId, id);
    }

    @Override
    public ResultVo<Integer> delStockCategory(int id) {
        Integer count = stockRepository.getCategorySub(id);
        if (count > 0) return new ResultVo<>(EnumResultVo.HasAssociatedData, "分类存在下级分类，不能删除");
        count = stockRepository.getCategoryGoodsSize(id);
        if (count > 0) return new ResultVo<>(EnumResultVo.HasAssociatedData, "分类关联商品，不能删除");
        stockRepository.delStockCategory(id);
        return new ResultVo<>(EnumResultVo.SUCCESS);
    }

    @Override
    public PagingResponse<ErpGoodsListVo> getGoodsList(int pageIndex, int pageSize, Integer contactId, String number, Integer categoryId, Integer status, String attr1, String attr4) {
        return stockRepository.getGoodsList(pageIndex, pageSize,contactId, number,categoryId,status,attr1,attr4);
    }

    @Override
    public List<ErpGoodsListVo> getGoodsListExport(String number, Integer categoryId, Integer status, String attr1, String attr4) {
        return stockRepository.getGoodsListExport(number,categoryId,status,attr1,attr4);
    }

    @Override
    public ErpGoodsListVo getGoodsList_1(Integer categoryId, String attr1, String attr2, String attr4) {
        return stockRepository.getGoodsList_1(categoryId,attr1,attr2,attr4);
    }



//    @Override
//    public List<ErpInviceListVo> getGoodsListN( String str) {
//        return stockRepository.getGoodsListN(str);
//    }

    @Override
    public String getCategoryString() {
        return stockRepository.getCategoryString();
    }

    @Override
    public Integer editGoods(String goodsName, String number, Integer categoryId, Integer unitId, Integer locationId, Integer reservoirId, Integer shelfId, Integer goodsId) {
        if (goodsId > 0) {
            stockRepository.updateGoods(goodsName, categoryId, unitId, locationId, reservoirId, shelfId, goodsId);
            return 0;
        }
        return stockRepository.saveGoods(goodsName, number, categoryId, unitId);
    }

    @Override
    public String getCategoryNameById(Integer id) {
        return stockRepository.getCategoryNameById(id);
    }

//    @Override
//    @Transactional(rollbackFor = Exception.class)
//    public Integer editGoodsSpec(String specNumber, String specName, String lowQty, String highQty, Integer goodsId, Integer specId, Integer locationId) {
//        Integer re = 0;
//        if (specId > 0) {
//            stockRepository.updateGoodsSpec(specName, lowQty, highQty, specId, locationId); //更新规格信息
//            re = 0;
//        } else {
//            re = stockRepository.saveGoodsSpec(specNumber, specName, lowQty, highQty, goodsId, locationId); //新增规格信息
////            List<ErpStockLocationEntity> entities = storeHouseRepository.getList(); //仓库列表
//            //新增库存查询信息
////            for (ErpStockLocationEntity entity : entities) {
//            stockRepository.saveGoodsStockInfo(goodsId, "", re, specNumber, locationId);
////            }
//        }
//
//        return re;
//    }

    @Override
    public Integer ifExsitNumber(String number) {
        return stockRepository.ifExsitNumber(number);
    }

    @Override
    public Integer ifExsitSpecNumber(String specNumber) {
        return stockRepository.ifExsitSpecNumber(specNumber);
    }

    @Override
    public ResultVo<Integer> deleteGoods(Integer goodsId) {
        stockRepository.deleteGoods(goodsId);
        return new ResultVo<>(EnumResultVo.SUCCESS);
    }

    @Override
    public Long getGooodsById(Integer goodsId) {
        return stockRepository.getGooodsById(goodsId);
    }

    @Override
    public Integer getCateIdByNameAndParentId(Integer id, String name) {
        return stockRepository.getCateIdByNameAndParentId(id, name);
    }

    @Override
    public List<UnitEntiy> getUnitList() {
        return stockRepository.getUnitList();
    }

    @Override
    public SpecRe getSpeckListById(Integer id) {
        return stockRepository.getSpeckListById(id);
    }

    @Override
    public PagingResponse<ErpGoodsSpecStockVo> goodsStockInfoSearch(int pageIndex, int pageSize, String locationNum, Integer houseId) {
        return stockRepository.goodsStockInfoSearch(pageIndex, pageSize, locationNum, houseId);
    }

    @Override
    public PagingResponse<ErpGoodsSpecStockVo> goodsSkuSearch(int pageIndex, int pageSize, String number,int filter0,Integer categoryId) {
        return stockRepository.goodsSkuSearch(pageIndex,pageSize,number,filter0,categoryId);
    }

    @Override
    public List<ErpGoodsSpecStockVo> goodsSkuListExport() {
        return stockRepository.goodsSkuListExport();
    }

    @Override
    public PagingResponse<ErpGoodsStockLogsListVo> goodsSkuSearchLogs(int pageIndex, int pageSize, String number) {
        return stockRepository.goodsSkuSearchLogs(pageIndex,pageSize,number);
    }

    @Override
    public List<ErpGoodsSpecStockVo> goodsStockInfoSearch(String str, Integer houseId) {
        return stockRepository.goodsStockInfoSearch(str,houseId);
    }

    @Override
    public List<ErpGoodsSpecEntity> getGoodSpesByYzItemId(Long yzItemId) {
        return stockRepository.getGoodSpesByYzItemId(yzItemId);
    }

//    @Override
//    public List<ErpGoodsSpecVo> getList(Integer locationId, Integer rreservoirId, Integer shelfId, String number) {
//        return stockRepository.getList(locationId, rreservoirId, shelfId, number);
//    }

    @Override
    public List<ErpGoodsSpecVo> getGoodSpecByMoveSpec(Integer locationId, Integer rreservoirId, Integer shelfId, String number) {
        return stockRepository.getGoodSpecByMoveSpec(locationId,rreservoirId,shelfId,number);
    }

    @Override
    public ResultVo<Integer> addGoodsSpecMoveForm(String[] ids,String[] number,String[] remarks, Integer outLocaltionId, Integer inLocationId, String moveNo,  String createBy) {
        return stockRepository.addGoodsSpecMoveForm(ids,number,remarks,outLocaltionId,inLocationId,moveNo,createBy);
    }

    @Override
    public PagingResponse<ErpGoodsSpecMoveFromEntiy> getStockHouseVo(Integer pageIndex, Integer pageSize,String moveNo) {
        return stockRepository.getStockHouseVo(pageIndex, pageSize,moveNo);
    }

    @Override
    public ErpGoodsSpecMoveFromEntiy getIdByErpGoodsSpecMoveFromEntiy(Integer id) {
        return stockRepository.getIdByErpGoodsSpecMoveFromEntiy(id);
    }

    @Override
    public List<ErpGoodsSpecVo> getGoodSpecByGoodId(Integer goodId) {
        return stockRepository.getGoodSpecByGoodId(goodId);
    }

    @Override
    public ResultVo<Integer> updErpGoodSpecById(Integer oldId, Integer newId) {
        return stockRepository.updErpGoodSpecById(oldId,newId);
    }

    @Override
    public List<ErpGoodsSpecStockListVo> searchGoodSpecStockByNumber(String number) {
        return stockRepository.searchGoodSpecStockByNumber(number);
    }

    @Override
    public List<ErpGoodsSpecStockListVo> getStockQtyByGoodsCategory() {
        return stockRepository.getStockQtyByGoodsCategory();
    }

//    @Override
//    public ResultVo<Long> getStockQtyByGoodsCategory(Integer categoryId) {
//        return stockRepository.getStockQtyByGoodsCategory(categoryId);
//    }

    @Override
    public List<ErpGoodsSpecEntity> specStockByExport() {
        return stockRepository.specStockByExport();
    }
}
