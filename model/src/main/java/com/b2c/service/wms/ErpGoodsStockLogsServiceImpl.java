package com.b2c.service.wms;

import com.b2c.entity.result.PagingResponse;
import com.b2c.repository.erp.ErpGoodsStockLogsRepository;
import com.b2c.interfaces.wms.ErpGoodsStockLogsService;
import com.b2c.entity.enums.erp.EnumGoodsStockLogSourceType;
import com.b2c.entity.enums.erp.EnumGoodsStockLogType;
import com.b2c.entity.vo.ErpGoodsStockLogsListVo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-10-08 17:06
 */
@Service
public class ErpGoodsStockLogsServiceImpl implements ErpGoodsStockLogsService {
    @Autowired
    private ErpGoodsStockLogsRepository repository;

    @Override
    public PagingResponse<ErpGoodsStockLogsListVo> getListByType(EnumGoodsStockLogType type, Integer pageIndex, Integer pageSize,Integer startTime,Integer endTime) {
        if (pageIndex == null || pageIndex <= 0) pageIndex = 1;
        if (pageSize == null || pageSize <= 0) pageSize = 20;
        return repository.getListByType(type, pageIndex, pageSize, startTime, endTime);
    }

    @Override
    public PagingResponse<ErpGoodsStockLogsListVo> getListByTypeAndSource(EnumGoodsStockLogType type, EnumGoodsStockLogSourceType sourceType, Integer pageIndex, Integer pageSize) {
        if (pageIndex == null || pageIndex <= 0) pageIndex = 1;
        if (pageSize == null || pageSize <= 0) pageSize = 20;
        return repository.getListByTypeAndSource(type, sourceType, pageIndex, pageSize);
    }

    @Override
    public PagingResponse<ErpGoodsStockLogsListVo> getStockInListBySourceType(Integer pageIndex, Integer pageSize, EnumGoodsStockLogSourceType sourceType, String goodsNumber, String specNumber) {
        return repository.getStockInListBySourceType(pageIndex, pageSize, sourceType, goodsNumber, specNumber);
    }

    @Override
    public List<ErpGoodsStockLogsListVo> getStockInListBySourceTypeForExcel(EnumGoodsStockLogSourceType sourceType, String goodsNumber, String specNumber) {
        return repository.getStockInListBySourceTypeForExcel(sourceType, goodsNumber, specNumber);
    }

    @Override
    public PagingResponse<ErpGoodsStockLogsListVo> getStockOutListBySourceType(Integer pageIndex, Integer pageSize, EnumGoodsStockLogSourceType sourceType, String goodsNumber, String specNumber) {
        return repository.getStockOutListBySourceType(pageIndex, pageSize, sourceType, goodsNumber, specNumber);
    }

    @Override
    public List<ErpGoodsStockLogsListVo> getStockOutListBySourceTypeForExcel(EnumGoodsStockLogSourceType sourceType, String goodsNumber, String specNumber) {
        return repository.getStockOutListBySourceTypeForExcel(sourceType, goodsNumber, specNumber);
    }
}
