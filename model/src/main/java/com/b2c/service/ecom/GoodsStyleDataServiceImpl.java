package com.b2c.service.ecom;

import javax.annotation.Resource;

import com.b2c.interfaces.ecom.GoodsStyleDataService;
import org.springframework.stereotype.Service;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ecom.GoodsStyleDataEntity;
import com.b2c.repository.ecom.GoodsStyleDataRepository;

@Service
public class GoodsStyleDataServiceImpl implements GoodsStyleDataService {
    @Resource
    private GoodsStyleDataRepository repository;

    @Override
    public PagingResponse<GoodsStyleDataEntity> getList(Integer pageIndex, Integer pageSize, String num,
            String platform) {

        return repository.getList(pageIndex, pageSize, num, platform);
    }

    @Override
    public void add(GoodsStyleDataEntity entity) {
        repository.add(entity);
    }
    
}
