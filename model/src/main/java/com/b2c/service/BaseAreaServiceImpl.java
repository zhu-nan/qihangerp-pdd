package com.b2c.service;

import com.b2c.entity.result.EnumResultVo;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.BaseAreaEntity;
import com.b2c.interfaces.BaseAreaService;
import com.b2c.repository.BaseAreaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 描述：
 * 基础地区Service
 *
 * @author qlp
 * @date 2019-01-30 14:02
 */
@Service
public class BaseAreaServiceImpl implements BaseAreaService {
    @Autowired
    private BaseAreaRepository repository;

    @Override
    public ResultVo<List<BaseAreaEntity>> getListByParent(String parentCode) {
        List<BaseAreaEntity> list = repository.getAreaListByParent(parentCode);
        if (list == null || list.size() == 0) {
            return new ResultVo<>(EnumResultVo.NotFound, "");
        } else {
            return new ResultVo<>(EnumResultVo.SUCCESS, list);
        }
    }
}
