package com.b2c.service.dou;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.DataRow;
import com.b2c.entity.douyin.*;
import com.b2c.entity.result.ResultVo;
import com.b2c.repository.oms.DouyinOrderRepository;
import com.b2c.repository.oms.OrderConfirmRepository;
import com.b2c.interfaces.dou.DouyinOrderService;
import com.b2c.entity.vo.DyOrderImportOrderVo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DouyinOrderServiceImpl implements DouyinOrderService {
    @Autowired
    private DouyinOrderRepository repository;

    @Autowired
    private OrderConfirmRepository confirmRepository;

    @Override
    public ResultVo<Integer> editDouYinOrder(DcDouyinOrdersEntity order, Integer flag) {
        return repository.editDouYinOrder(order,flag);
    }

    @Override
    public PagingResponse<DcDouyinOrdersListVo> getDouyinOrders(Long  shopId, Integer pageIndex, Integer pageSize, String orderNum, Integer startTime, Integer endTime, Integer state, Integer auditStatus, String logisticsCode) {
        return repository.getDouyinOrders(shopId,pageIndex,pageSize,orderNum,startTime,endTime,state,auditStatus,logisticsCode);
    }

    @Override
    public List<DcDouyinOrdersListVo> getDouyinOrders(Integer shopId, Integer startTime, Integer endTime,
            Integer status) {
      
        return repository.getDouyinOrders(shopId, startTime, endTime, status);
    }
    @Override
    public List<DcDouyinOrdersItemsEntity> getDouyinOrdersExport(Integer startTime, Integer endTime, Integer status) {
        return repository.getDouyinOrdersExport(startTime,endTime,status);
    }

    @Override
    public DcDouyinOrdersListVo getOderDetailByOrderId(Long orderId) {
        return repository.getOderDetailByOrderId(orderId);
    }

    @Override
    public DcDouyinOrdersListVo getOderDetailByOrderIdForSend(Long orderId) {
        return repository.getOderDetailByOrderIdForSend(orderId);
    }

    @Override
    public List<DcDouyinOrdersItemsEntity> getOderDetailByPrint(String logisticsCode, Integer print) {
        return repository.getOderDetailByPrint(logisticsCode,print);
    }

    @Override
    public ResultVo<Integer> douyinOrderAffirm(Long dcDouyinOrdersId, Integer clientId, String receiver, String mobile, String address, String sellerMemo) {
        return confirmRepository.douyinOrderAffirm(dcDouyinOrdersId,clientId,receiver,mobile,address,sellerMemo);
    }

    @Override
    public ResultVo<Long> updOrderSpec(Long orderItemId, Integer erpGoodSpecId, Integer quantity) {
        return repository.updOrderSpec(orderItemId,erpGoodSpecId,quantity);
    }

    @Override
    public void updDouyinOrderStatus(String orderId,Integer orderStatus) {
        repository.updDouyinOrderStatus(orderId,orderStatus);
    }

    @Override
    public ResultVo<Long> addOrderGift(Long orderId, Integer erpGoodsId, Integer erpGoodSpecId, Integer quantity) {
        return repository.addOrderGift(orderId,erpGoodsId,erpGoodSpecId,quantity);
    }

    @Override
    public ResultVo<Integer> editDouYinRefundOrder(DcDouyinOrdersRefundEntity order) {
        return repository.editDouYinRefundOrder(order);
    }

    @Override
    public PagingResponse<DouyinOrdersRefundEntity> getDouyinRefundOrders(Integer shopId, Integer pageIndex, Integer pageSize, String orderNum, String logisticsCode, Integer startTime, Integer endTime, Integer state, Integer aftersaleType) {
        return repository.getDouyinRefundOrders(shopId,pageIndex,pageSize,orderNum,logisticsCode,startTime,endTime,state,aftersaleType);
    }

    @Override
    public ResultVo<Integer> confirmRefund(Long id,String logisticsCompany,String logisticsCode) {
        return repository.confirmRefund(id,logisticsCompany,logisticsCode);
    }

    @Override
    public DouyinOrdersRefundEntity getDouYinRefundOrderDetail(Long refundId) {
        return repository.getDouYinRefundOrderDetail(refundId);
    }

    @Override
    public void updOrderLogisticsCode(Long orderId, String logisticsId, String logisticsCode,Integer print) {
        repository.updOrderLogisticsCode(orderId,logisticsId,logisticsCode,print);
    }

    @Override
    public PagingResponse<DcDouyinOrdersItemsEntity> getDouyinPrintOrders(Integer dyShopId,Integer pageIndex, Integer pageSize, String orderNum, Integer startTime, Integer endTime, String goodsNum, String skuNum,Integer print,String code,String startDate,String endDate,Integer isBz) {
        return repository.getDouyinPrintOrders(dyShopId,pageIndex,pageSize,orderNum,startTime,endTime,goodsNum,skuNum,print,code,startDate,endDate,isBz);
    }

    @Override
    public void updOrderPrint(Long orderId, String result, Integer orderStatus, Integer print) {
        repository.updOrderPrint(orderId,result,orderStatus,print);
    }

    @Override
    public void dyOrderCancelNotify(Long orderId, Integer status, String cancelReason) {
        repository.dyOrderCancelNotify(orderId, status, cancelReason);
    }

    @Override
    public ResultVo<List<DcDouyinOrdersItemsEntity>> getPrintOrderList(Long orderId, Integer isHebing, Integer print) {
        return repository.getPrintOrderList(orderId,isHebing,print);
    }

    @Override
    public ResultVo<Integer> orderSend(DcDouyinOrdersListVo order) {
        return repository.orderSend(order);
    }
    /**
     * 
     * 订单手动发货
     * @param id
     * @param company
     * @param code
     * @return
     */
    @Override
    public ResultVo<Integer> orderHandSend(Long id,String company,String code){
        return repository.orderHandSend(id, company, code);
    }

    @Override
    public ResultVo<Integer> cancelOrderPrint(Long orderId) {
        return repository.cancelOrderPrint(orderId);
    }

    @Override
    public void updRefundOrderAuditStatus(Long orderId, Integer auditStatus) {
        repository.updRefundOrderAuditStatus(orderId,auditStatus);
    }

    @Override
    public List<DcDouyinOrdersListVo> getDouyinOrderHebing(Integer dyShopId) {
        return repository.getDouyinOrderHebing(dyShopId);
    }

    @Override
    public void updOrderPrintByCode(String logisticsCode, Integer orderStatus, String result, Integer print) {
        repository.updOrderPrintByCode(logisticsCode,orderStatus,result,print);
    }

    @Override
    public ResultVo<Integer> ordersUpdSku(ArrayList orderIds, String oldSku, String newSku) {
        return repository.ordersUpdSku(orderIds,oldSku,newSku);
    }

    @Override
    public ResultVo<Integer> ordersUpdSkuById(Long id, String newSku) {
        return repository.ordersUpdSkuById(id,newSku);
    }

    @Override
    public void zhuBoOrderSettle(List<Long> ids, Integer isSettle) {
        repository.zhuBoOrderSettle(ids,isSettle);
    }

    @Override
    public List<DouyinOrderStatis> douyinOrderStatis(String startTime, String endTime) {
        return repository.douyinOrderStatis(startTime,endTime);
    }

    @Override
    public void test(List<DataRow> datas) {
        repository.test(datas);
    }

    @Override
    public Long getDyOrderTime(Integer shopId) {
        return repository.getDyOrderTime(shopId);
    }

    @Override
    public PagingResponse<DouyinOrderStatisticsEntity> getOrderStatisticsList(Integer pageIndex, Integer pageSize, String orderDate, Integer shopId, Long authorId) {
        return repository.getOrderStatisticsList(pageIndex,pageSize,orderDate,shopId,authorId);
    }

    @Override
    public List<DouyinOrderStatisticsEntity> getOrderStatisticsListExport(String orderDate, Integer shopId, Long authorId) {
        return repository.getOrderStatisticsListExport(orderDate,shopId,authorId);
    }

    @Override
    public ResultVo<String> importExcelOrder(List<DyOrderImportOrderVo> orderList, Integer shopId) {
   
        return repository.importExcelOrder(orderList, shopId);
    }

    @Override
    public ResultVo<Integer> updOrderAddress(Long id, String receiverName, String receiverPhone,
            String receiverAddress) {
        return repository.updOrderAddress(id, receiverName, receiverPhone, receiverAddress);
    }

    @Override
    public ResultVo<Integer> handAddRefund(Long orderItemId,Long refundId){
        return repository.handAddRefund(orderItemId, refundId);
    }

    @Override
    public void editRefundLogisticsCode(Long refundId, String company, String code, String logisticsTime) {
        repository.editRefundLogisticsCode(refundId, company, code, logisticsTime);
    }

}
