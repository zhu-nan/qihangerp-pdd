package com.b2c.service.dou;

import com.b2c.repository.DYOrderPullLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DYOrderPullLogService {
    @Autowired
    private DYOrderPullLogRepository repository;

    public void insertLog(String pullType,Integer orderType,Integer dataCount,String dataStartTime,String dataEndTime,String dataResult){
        repository.insertLog(pullType,orderType, dataCount, dataStartTime, dataEndTime, dataResult);
    }
}
