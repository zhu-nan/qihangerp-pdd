package com.b2c.service.pdd;

import com.b2c.entity.pdd.PddMarketingFeeEntity;
import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.result.ResultVo;
import com.b2c.repository.pdd.PddMarketingFeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PddMarketingFeeServiceImpl implements PddMarketingFeeService {

    @Autowired
    private PddMarketingFeeRepository repository;
    @Override
    public PagingResponse<PddMarketingFeeEntity> getList(Integer pageIndex, Integer pageSize,Integer shopId,Integer type, String startTime, String endTime) {
        return repository.getList(pageIndex, pageSize,shopId,type, startTime, endTime);
    }

    @Override
    public ResultVo<String> importExcelFeeList(List<PddMarketingFeeEntity> list, Integer shopId) {
        return repository.importExcelFeeList(list,shopId);
    }
}
