package com.b2c.service.erp;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.GoodsCategoryAttributeEntity;
import com.b2c.entity.GoodsSpecAttrEntity;
import com.b2c.entity.erp.ErpGoodsBrandEntity;
import com.b2c.entity.erp.ErpGoodsEntity;
import com.b2c.entity.erp.ErpGoodsSpecEntity;
import com.b2c.entity.erp.GoodsCategoryEntity;
import com.b2c.entity.erp.vo.*;
import com.b2c.entity.result.ResultVo;
import com.b2c.repository.erp.ErpGoodsRepository;
import com.b2c.interfaces.erp.ErpGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 描述：
 * 商品Service
 *
 * @author qlp
 * @date 2019-03-21 17:00
 */
@Service
public class ErpGoodsServiceImpl implements ErpGoodsService {
    @Autowired
    private ErpGoodsRepository repository;

    @Override
    public PagingResponse<ErpGoodsEntity> getList(Integer pageIndex, Integer pageSize, String goodsNumber,Integer isDelete) {
        return repository.getList(pageIndex, pageSize, goodsNumber, isDelete);
    }

    @Override
    public PagingResponse<GoodsSearchShowVo> getDySalesList(Long shopId, Integer pageIndex, Integer pageSize, String goodsNumber, Integer startTime, Integer endTime) {
        return repository.getDySalesList(shopId,pageIndex,pageSize,goodsNumber,startTime,endTime);
    }

    @Override
    public List<ErpGoodsEntity> getListFor7Day() {
        return repository.getListFor7Day();
    }

    @Override
    public List<GoodsSearchShowVo> getGoodsSpecByNumberForPurchase(String number, int limit) {
        return repository.getGoodsSpecByNumberForPurchase(number, 0, limit);
    }


    @Override
    public List<GoodsSearchByNumberVo> getGoodsByNumber(String number) {
        return repository.getGoodsByNumber(number);
    }

    @Override
    public ErpGoodsEntity getGoodsEntityByNumber(String number) {
        return repository.getGoodsEntityByNumber(number);
    }

    @Override
    public List<ErpGoodsSpecListVo> getSpecByGoodsId(Integer goodsId) {
        return repository.getSpecByGoodsId(goodsId);
    }

    @Override
    public List<ErpGoodsSpecEntity> getSpecListByGoodsId(Integer goodsId) {
        return repository.getSpecListByGoodsId(goodsId);
    }

    @Override
    public PagingResponse<ErpGoodsSpecListVo> getSpecByGoodsNumber(Integer pageIndex,Integer pageSize,String goodsNumber) {
        return repository.getSpecByGoodsNumber(pageIndex,pageSize,goodsNumber);
    }

//    @Override
//    public StockOrdersVo getStockOrders(Integer orderId, Integer erpInvoiceId) {
//        return repository.getStockOrders(orderId, erpInvoiceId);
//    }

    @Override
    public ResultVo<Integer> goodsExcelBatchAdd(List<GoodsExcelVo> goodsList) {
        return repository.goodsExcelBatchAdd(goodsList);
    }

//    @Override
//    public ResultVo<Integer> goodsAddNew(List<GoodsExcelVo> goodsList) {
//        return repository.goodsAddNew(goodsList);
//    }

    @Override
    public GoodsSpecDetailVo getGoodsSpecDetailByNumber(String number) {
        return repository.getGoodsSpecDetailByNumber(number);
    }

    @Override
    public int getTotalGoods() {
        return repository.getTotalGoods();
    }

    @Override
    public ResultVo<Integer> goodsAdd(ErpGoodsAddVo goodsAddVo) {
        return repository.goodsAdd(goodsAddVo);
    }

    @Override
    public ResultVo<Integer> goodsAddSpec(ErpGoodsAddVo goodsAddVo) {
        return repository.goodsAddSpec(goodsAddVo);
    }

    @Override
    public ErpGoodsEntity getById(Integer id) {
        return repository.getById(id);
    }

    @Override
    public ErpGoodsSpecEntity getSpecByNumber(String specNumber) {
        return repository.getSpecByNumber(specNumber);
    }

    @Override
    public ErpGoodsSpecEntity getSpecBySpecId(Integer specId) {
        return repository.getSpecBySpecId(specId);
    }

    @Override
    public List<GoodsSpecAttrEntity> getSkuValueList(String type, Integer goodsId) {
        return repository.getSkuValueList(type, goodsId);
    }

    @Override
    public ResultVo<Integer> goodsBaseInfoEdit(Integer goodsId, ErpGoodsAddVo goodsAddVo) {
        return repository.goodsBaseInfoEdit(goodsId, goodsAddVo);
    }

    @Override
    public ErpGoodsEntity getErpGoodsEntity(Integer id) {
        return repository.getErpGoodsEntity(id);
    }

    @Override
    public void priceEdit(Integer goodsId, Float wholesalePrice, Float salePrice) {
        repository.priceEdit(goodsId, wholesalePrice, salePrice);
    }

//    @Override
//    public void updErpGoodsByOGoods() {
//        repository.updErpGoodsByOGoods();
//    }
      @Override
    public List<GoodsCategoryAttributeEntity> getSpecByCategory(Integer categoryId) {
        return repository.getAttributeByCategory(categoryId, 1);
    }

    @Override
    public List<ErpGoodsBrandEntity> getBrandList() {
        return repository.getBrandList();
    }

    @Override
    public void updErpGoodSpecAttr(Integer specId, String attr1, String attr2) {
        repository.updErpGoodSpecAttr(specId,attr1,attr2);
    }

    @Override
    public Integer getGoodsSpecStockById(Integer specId) {
        return repository.getGoodsSpecStockById(specId);
    }

    @Override
    public void updateSpecPurPrice(Integer specId, Double purPrice) {
        repository.updateSpecPurPrice(specId, purPrice);
    }

    @Override
    public List<GoodsCategoryEntity> getCategoryListByParentId(Integer parendId) {

        return repository.getCategoryListByParentId(parendId);
    }
}
