package com.b2c.interfaces.wms;

import com.b2c.entity.result.ResultVo;
import com.b2c.entity.vo.ErpCheckoutStockInItemVo;
import com.b2c.entity.vo.ErpPurchaseStockInItemVo;

import java.util.List;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-09-20 09:47
 */
public interface StockInService {

    /***
     * 验货单入库
     * @param checkoutId 验货单id
     * @param items 入库明细items
     * @param stockInUserId 入库userid
     * @param stockInUserName 入库username
     * @return
     */
//    ResultVo<Long> checkoutStockIn(Long checkoutId, List<ErpCheckoutStockInItemVo> items, Integer stockInUserId, String stockInUserName);
    ResultVo<Long> purchaseStockIn(Long invoiceId,Integer qcReport,String qcInspector, List<ErpPurchaseStockInItemVo> items, Integer stockInUserId, String stockInUserName);

    /***
     * 验货入库数据校验
     * @param checkoutId
     * @param items
     * @return
     */
    ResultVo<Long> checkoutStockInDataChecked(Long checkoutId, List<ErpCheckoutStockInItemVo> items);
    ResultVo<String> purchaseStockInDataChecked(Long invoiceId, List<ErpPurchaseStockInItemVo> items);
}
