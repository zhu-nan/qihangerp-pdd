package com.b2c.interfaces.xhs;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.xhs.XhsGoodsEntity;

public interface XhsGoodsService {

    ResultVo<Long> pullGoods(XhsGoodsEntity goodsEntity);
    PagingResponse<XhsGoodsEntity> getGoodsList(Integer shopId, Integer pageIndex, Integer pageSize, String goodsNum);
    ResultVo<Integer> linkErpGoodsSpec(Long xhsGoodsSpecId,String erpCode);

    /**
     * 查询商品信息，根据小红书商品规格itemId
     * @param xhsSkuId
     * @return
     */
    XhsGoodsEntity getGoodsSpecByXhsSkuId(String xhsSkuId);
}
