package com.b2c.interfaces.xhs;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.xhs.XhsRefundEntity;

public interface XhsRefundService {
     ResultVo<Long> pullRefund(XhsRefundEntity refund);
     PagingResponse<XhsRefundEntity> getRefundList(Integer shopId, Integer pageIndex, Integer pageSize);
     XhsRefundEntity getRefundDetailById(Long id);

     /**
      * 处理仅退款
      * @param id
      */
     void agreeRefundOnly(Long id);
}
