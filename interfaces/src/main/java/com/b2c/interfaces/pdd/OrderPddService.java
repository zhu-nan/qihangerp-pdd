package com.b2c.interfaces.pdd;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.WaitSendGoodsSpecModel;
import com.b2c.entity.vo.ExcelOrderFileLogs;
import com.b2c.entity.apitao.PddOrderStatisVo;
import com.b2c.entity.apitao.UpdOrderTagReq;
import com.b2c.entity.pdd.OrderPddEntity;
import com.b2c.entity.pdd.OrderPddItemEntity;
import com.b2c.entity.pdd.OrderViewModel;
import com.b2c.entity.pdd.PddPrintOrderVo;
import com.b2c.entity.pdd.RefundPddEntity;
import com.b2c.entity.result.ResultVo;

import java.util.ArrayList;
import java.util.List;

public interface OrderPddService {
    /**
     * 拦截订单
     * @param orderSn
     * @param refundId
     */
    ResultVo<Long> interceptOrder(String orderSn, Long refundId,String msg,Integer afterSalesType);


    ResultVo<Long> insertOrder(OrderPddEntity order, Integer shopId);

    /**
     * 插入拼多多订单消息推送
     * @param shopId
     * @param tid
     * @return
     */
    ResultVo<Long> insertOrderForMessage(Integer shopId,String tid);
    ResultVo<Integer> updateOrder(Integer orderStatus, String company, String number, String shipping_time, Integer refundStatus, Integer afterStatus, String remark, String orderSn, String lastShipTime, Integer shopId);

    public ResultVo<Long> editPddOrder(OrderPddEntity order, Integer shopId);
    OrderPddEntity queryOrderBySn(String orderSn);

    /**
     * 添加到平台订单系统
     * @param order
     * @return
     */
//    ResultVo<ErpSalesPullCountResp> editErpSalesOrder(OrderPddEntity order,Integer shopId);
    /**
     * pdd退货
     * @param r
     * @return
     */
    ResultVo<Integer> editRefundPddOrder(RefundPddEntity r);
    ResultVo<Integer> updRefundPddOrder(RefundPddEntity r);

    /**
     * 查询订单信息(包含order_item)
     * @param pageIndex
     * @param pageSize
     * @param orderSn
     * @param status
     * @param shopId
     * @param startTime
     * @param endTime
     * @return
     */
    PagingResponse<OrderPddEntity> getOrderListAndItem(Integer pageIndex, Integer pageSize, String orderSn, Integer status, Integer shopId,Integer startTime, Integer endTime, Integer auditStatus,String pddGoodsId);

    /**
     * 查询所有订单（3个月内）
     * @param shopId
     * @param orderStatus
     * @param refundStatus
     * @return
     */
    PagingResponse<OrderPddEntity> getOrderListByStatus(Integer pageIndex, Integer pageSize, Integer shopId, Integer orderStatus, Integer refundStatus);

    /**
     * 查询订单信息(不包含order_item)
     * @param pageIndex
     * @param pageSize
     * @param orderSn
     * @param status
     * @param shopId
     * @param tags
     * @param startTime
     * @param endTime
     * @param auditStatus
     * @return
     */
    PagingResponse<OrderPddEntity> getOrderList(Integer pageIndex, Integer pageSize, String orderSn, Integer status, Integer shopId, List<Integer> tags, Integer startTime, Integer endTime, Integer auditStatus);

    /**
     * 获取订单展示列表（order_item left join orders）
     * @param pageIndex
     * @param pageSize
     * @param orderSn
     * @param goodsSpecNum
     * @param status
     * @param shopId
     * @param startTime
     * @param endTime
     * @param orderBy 排序方式（1按承诺发货时间降序2按下单时间降序）
     * @return
     */
    PagingResponse<OrderViewModel> getOrderViewList(Integer pageIndex, Integer pageSize, String orderSn, String goodsSpecNum, Integer status, Integer shopId, Integer startTime, Integer endTime, Integer printStatus, int orderBy, String trackingNumber);
    PagingResponse<OrderViewModel> getOrderViewList(Integer pageIndex, Integer pageSize, String orderSn, String goodsSpecNum, Integer status, Integer refundStatus, Integer shopId, Integer startTime, Integer endTime, Integer printStatus, int orderBy, String trackingNumber);
    PagingResponse<OrderViewModel> getOrderViewList(Integer pageIndex, Integer pageSize, String orderSn, String goodsSpecNum, Integer status, Integer shopId, Integer startTime, Integer endTime, Integer printStatus, int orderBy, String trackingNumber, String goodNum);

    public PagingResponse<OrderViewModel> getOrderViewListByPrint(Integer pageIndex, Integer pageSize, Integer shopId, Integer printStatus, String trackingNumber, String printStartTime, String printEndTime);

    public PagingResponse<OrderViewModel> getOrderViewListPrintWdy(Integer shopId, Integer pageIndex, Integer pageSize, String orderSn, String goodsNum, String goodsSpecNum, Integer printStatus, String startTime, String endTime);

    /**
     * 查询合并待打单
     * @param shopId
     * @return
     */
    public List<OrderPddEntity> getPddOrderHebing(Integer shopId, Integer printStatus, String goodsNum);
    public List<OrderPddEntity> getPddOrderCodePrint(Integer shopId);
    public List<OrderPddEntity> getDshList(Integer shopId, Integer auditStatus, Long logId);

    /**
     * 获取订单详情
     * @param id
     * @return
     */
    OrderPddEntity getOrderDetailAndItemsById(Long id);

    /**
     * 确认拼多多订单
     * @param orderId
     * @param receiver
     * @param mobile
     * @param address
     * @param remark
     * @return
     */
     ResultVo<Integer> orderConfirmAndJoinDeliveryQueueForPdd(Long orderId, String receiver, String mobile, String address, String remark);
     public ResultVo<Integer>  orderBatchConfirm(Integer shopId, String startDate, String endDate);
    /**
     * 修改订单item商品
     * @param orderItemId
     * @param erpGoodSpecId
     * @param quantity
     * @return
     */
    ResultVo<Long> updOrderSpec(Long orderItemId, Integer erpGoodSpecId, Integer quantity);
    /**
     * 创建pdd订单
     * @param order
     * @return
     */
    ResultVo<Integer> orderCreatePdd(OrderPddEntity order);
    /**
     * 添加订单赠品
     * @param orderId
     * @param erpGoodsId
     * @param erpGoodSpecId
     * @param quantity
     * @return
     */
    ResultVo<Long> addOrderGift(Long orderId, Integer erpGoodsId, Integer erpGoodSpecId, Integer quantity);
    /**
     * 更新快递公司
     * @param express
     */
    void initExpress(String express);
    /**
     * 查询快递公司id
     * @param logisticsCompany
     * @return
     */
    Integer getPddLogisticsCompanyId(String logisticsCompany);
    /**
     * 更新Pdd订单状态
     * @param orderId
     */
    void updPddOrderStats(Long orderId);

    /**
     * 拼多多接口更新订单状态
     * @param orderSN
     * @param orderStatus
     * @param refundStatus
     */
    void updPddOrderStatus(String orderSN, Integer orderStatus, Integer refundStatus);

    public void delPddOrderItemIsGift(Long orderItemId);
    /**
     * 修改订单是否为刷单
     */
    public void updOderTag(UpdOrderTagReq req);
    /**
     * 创建出库单
     * @param startDate
     * @param endDate
     * @return
     */
     ResultVo<Integer> createStockOurForm(Integer shopId, String startDate, String endDate);
    /**
     * 获取pdd更新时间
     * @param shopId
     * @return
     */
    String getPddUpTodate(Integer shopId);
    /**
     * 查询订单统计数据
     * @param date
     * @return
     */
    PddOrderStatisVo getOrderStatic(String date);

    public Integer orderCount(String orderSn);
    public List<OrderPddEntity> getPddRefundList(Integer shopId, String startDate, String endDate);
    public OrderPddEntity getOrder(String orderNo);
    public OrderPddEntity getOrder(Long orderId);
    public ResultVo<Integer> orderConfirmPddExcelIn(Long orderId, String trackingCompany, String trackingNumber, String shippingTime, Long logId, String phone, String name);
    public Long addExcelOrderInLogs(Long id, Integer shopId, Integer totalCount, Integer successCount, Integer notFundCount, Integer failCount);
    /**
     * 订单日志列表
     * @param pageIndex
     * @param pageSize
     * @param shopId
     * @return
     */
    public PagingResponse<ExcelOrderFileLogs> getOrderListAndItem(Integer pageIndex, Integer pageSize, Integer shopId);

    /*public PagingResponse<OrderPddEntity> getDfhList(Integer pageIndex,Integer pageSize,Integer startTime,Integer endTime,Integer shopId,Integer printStatus);*/
    /**
     * 更新订单打印信息
     * @param company
     * @param code
     * @param encryptedData
     * @param signature
     */
    public ResultVo<Integer> updPddOrderPrint(OrderViewModel order, String company, String code, String encryptedData, String signature);
    public ResultVo<String> checkPrint(List<OrderViewModel> orders);

    /**
     * 统计待发货商品规格信息
     * @return
     */
    List<WaitSendGoodsSpecModel> getWaitSendGoodsSpecList();
    public ResultVo<PddPrintOrderVo> getPrintOrderList(String orderNo, Integer isHebing, String goodsNum);
    /**
     * 修改拼多碰订单处理结果
     * @param orderId
     * @param result
     */
    public void updPddOrderResult(Long orderId, String result, Integer printStatus, String printTime);
    /**
     * 重复打印订单
     * @param orderSn
     * @return
     */
    public List<OrderViewModel>  getPrintOrderRepeat(String orderSn);
    public List<OrderViewModel>  getPrintOrderRepeatNumber(String trackingNumber);
    /**
     * 查询合并选择订单
     * @param orderSnArr
     * @return
     */
    public ResultVo<List<OrderViewModel>>  getPrintOrderSelectList(ArrayList orderSnArr);
    public ResultVo<Integer> cancelOrderPrint(Long orderId);
    ResultVo<Integer> updOrderRemark(Long orderId,String remark);
    ResultVo<Integer> updOrderAddress(Long orderId,String receiverName,String receiverPhone,String receiverAddress);

    /**
     * 订单发货
     * @param order
     * @return
     */
    public ResultVo<Integer> orderSend(Long orderId);
    ResultVo<Integer> orderSendAndConfirm(OrderPddEntity order);

    public ResultVo<Long> updOrderRefundSpec(Long refundId, Integer erpGoodSpecId, Integer quantity);

    /***
     * 获取订单最后创建时间
     * @return
     */
    String getLashOrderCreateTime(Integer shopId);
    /**
     * 获取最后退款创建时间
     * @param shopId
     * @return
     */
    Long getLashRefundCreateTime(Integer shopId);

    /**
     * 
     * @param itemId
     * @return
     */
    OrderPddItemEntity getOrderItemByItemId(Long itemId);
}
