package com.b2c.interfaces.pdd;

import java.util.List;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.erp.vo.ShopGoodsSkuLinkErpSkuVo;
import com.b2c.entity.pdd.ShopGoodsEntity;
import com.b2c.entity.pdd.ShopGoodsSkuEntity;
import com.b2c.entity.result.ResultVo;

public interface ShopGoodsService {

     ResultVo<Long> addGoods(ShopGoodsEntity goodsEntity);

     ResultVo<Long> updatePublishTime(Long id,String publishTime);
     ResultVo<Long> updateRemark(Long id,String remark);
     ResultVo<Long> updateIsOnsale(Long id);
     ResultVo<Long> updateTotalSales(Long id,Integer totalSales);
     PagingResponse<ShopGoodsEntity> getGoodsList(Integer shopType,Integer shopId,Integer pageIndex, Integer pageSize, String num, Long goodsId, Integer isOnsale);
     ShopGoodsEntity getGoodsById(Long goodsId);
     List<ShopGoodsSkuEntity> getGoodsSkuListByGoodsId(Long goodsId);
     ResultVo<Integer> linkErpGoodsSpec(Long pddSkuId,String erpCode);
     ResultVo<Integer> upGoodsNumById(Long goodsId,String goodsNum);
     ResultVo<Integer> linkErpSkuAll(Long goodsId, List<ShopGoodsSkuLinkErpSkuVo> skuLinkList);
}
