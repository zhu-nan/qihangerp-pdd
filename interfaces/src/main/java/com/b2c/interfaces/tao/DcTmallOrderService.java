package com.b2c.interfaces.tao;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.apitao.DcTmallOrderCostEntity;
import com.b2c.entity.apitao.TaoOrderSalesDataEntity;
import com.b2c.entity.apitao.UpdOrderTagReq;
import com.b2c.entity.query.OrderItemSwapReq;
import com.b2c.entity.query.TaoOrderQuery;
import com.b2c.entity.query.TmallOrderCostQuery;
import com.b2c.entity.datacenter.DcTmallOrderEntity;
import com.b2c.entity.datacenter.DcTmallOrderItemEntity;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.vo.erp.ErpSalesOrderDetailVo;
import com.b2c.entity.vo.OrderImportPiPiEntity;
import com.b2c.entity.vo.finance.FinanceOrderItemListVo;
import com.b2c.entity.vo.finance.FinanceOrderListVo;

import java.util.List;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-10-23 15:29
 */
public interface DcTmallOrderService {

    /**
     * 淘宝开放平台接口更新订单
     * @param shopId
     * @param order
     * @return
     */
    ResultVo<Integer> updateTmallOrderForOpenTaobao(Integer shopId, DcTmallOrderEntity order);

    /**
     * 分页获取tmall订单
     *
     * @param pageIndex
     * @param pageSize
     * @param orderId
     * @param status
     * @param receiverMobile 收货人手机号
     * @param startTime      开始时间
     * @param endTime        结束时间
     * @return
     */
//    PagingResponse<DcTmallOrderEntity> getList(EnumTmallOrderSource orderSource, Integer pageIndex, Integer pageSize, String orderId, Integer status, Integer shopId, String receiverMobile, String startTime, String endTime);

    PagingResponse<DcTmallOrderEntity> getList(Integer pageIndex, Integer pageSize, String orderId, Integer status, Integer shopId, String receiverMobile, Integer developerId, String startTime, String endTime);
    public PagingResponse<DcTmallOrderEntity> getNewList(TaoOrderQuery reqData);

    /**
     * 查询订单明细list（供财务使用）
     *
     * @param pageIndex
     * @param pageSize
     * @param orderId
     * @param sendStatus
     * @param receiverMobile
     * @param startTime
     * @param endTime
     * @return
     */
    PagingResponse<FinanceOrderItemListVo> getOrderItemList(Integer pageIndex, Integer pageSize, String orderId, Integer sendStatus, String receiverMobile, String specNumber, String startTime, String endTime);

    List<FinanceOrderItemListVo> getOrderItemListForExcel(String orderId, Integer sendStatus, String receiverMobile, String specNumber, String startTime, String endTime);

    /**
     * 获取订单（供财务使用）
     * @param pageIndex
     * @param pageSize
     * @param orderId
     * @param sendStatus
     * @param receiverMobile
     * @param startTime
     * @param endTime
     * @return
     */
    PagingResponse<FinanceOrderListVo> getOrderList(Integer pageIndex, Integer pageSize, String orderId, Integer sendStatus, String receiverMobile, Integer developerId, String startTime, String endTime);

    List<FinanceOrderListVo> getOrderListForExcel(String orderId, Integer sendStatus, String receiverMobile, Integer developerId, String startTime, String endTime);


    /**
     * 获取订单详情
     *
     * @param orderId
     * @return
     */
    DcTmallOrderEntity getOrderDetailAndItemsById(Long orderId);

    /**
     * 获取订单信息(包含收货地址)
     * @param orderId
     * @return
     */
    DcTmallOrderEntity getOrderEntityById(Long orderId);

    /**
     * 获取订单items
     *
     * @param orderId
     * @return
     */
    List<DcTmallOrderItemEntity> getOrderItemsByOrderId(Long orderId);

    /**
     * 订单确认并加入到仓库发货队列
     *
     * @param orderId
     * @return
     */
    ResultVo<Integer> orderConfirmAndJoinDeliveryQueueForTmall(Long orderId, Integer clientId, String receiver, String mobile, String address, String sellerMemo);

    /**
     * 修改商品最新规格
     *
     * @param itemId
     * @param specId
     */
    Integer updGoodTmallSpec(Integer itemId, Integer specId);

    /**
     * 天猫订单退货
     *
     * @param orderId
     * @param itemIds
     * @param nums
     * @return
     */
//    ResultVo<Integer> addTaoBaoOrderCancel(Long orderId, String[] itemIds, String[] nums);

    /**
     * 天猫订单退货(手动)
     *
     * @param orderItemId
     * @param refundId 退货id（淘宝店内的退货id）
     * @return
     */
   ResultVo<Long> addTaoOrderRefund(Long orderItemId, String refundId);

    /**
     * 查询天猫退货列表
     *
     * @param pageIndex
     * @param pageSize
     * @param orderNum
     * @param startTime
     * @param endTime
     * @param returnOrderNum
     * @param buyerName
     * @param state
     * @return
     */
//    PagingResponse<RefundOrderListVo> getTmallAfterOrders(Integer pageIndex, Integer pageSize, String orderNum, Integer startTime, Integer endTime, String returnOrderNum, String buyerName, Integer state, Integer orderSource);

    /**
     * 处理天猫订单退货
     *
     * @param id
     * @param state
     * @param exress
     * @return
     */
//    ResultVo<Integer> reviewRefundTmall(Long id, Integer state, ExpressInfoVo exress);


    /**
     * 批量更新收货地址
     *
     * @param orderList
     */
    void batchUpdateTmallOrderReceiver(List<DcTmallOrderEntity> orderList);

    /**
     * 修改订单item商品
     * @param orderItemId
     * @param erpGoodSpecId
     * @param quantity
     * @return
     */
    ResultVo<Long> updOrderSpec(Long orderItemId, Integer erpGoodSpecId, Integer quantity);
    /**
     * 修改订单标签
     */
    public void updOderTag(UpdOrderTagReq req);
    /**
     * 订单成本修改新增
     * @param costs
     */
    public void  updOrderTagCost(List<DcTmallOrderCostEntity> costs);
    /**
     * 成本列表
     * @return
     */
    public PagingResponse<DcTmallOrderCostEntity> orderCosts(TmallOrderCostQuery query);
    /**
     * 导入成本列表
     * @param costList
     */
    public void batchUpdateTmallOrderCost(List<DcTmallOrderCostEntity> costList);
    /**
     * 添加口罩刷单
     * @param salesOrder
     * @return
     */
    public ResultVo<Integer> addHySalesOrder(ErpSalesOrderDetailVo salesOrder);
    /**
     *
     * @param orderId
     * @param erpGoodsId
     * @param erpGoodSpecId
     * @param quantity
     * @return
     */
    public ResultVo<Long> addOrderGift(Long orderId, Integer erpGoodsId, Integer erpGoodSpecId, Integer quantity);
    /**
     * 分页查询订单统计明细
     * @param pageIndex
     * @param pageSize
     * @param startTime
     * @param endTime
     * @return
     */
    public PagingResponse<TaoOrderSalesDataEntity> salesOrderDataList(Integer shopId, Integer pageIndex, Integer pageSize, String startTime, String endTime);
    /**
     * 换货
     */
    public ResultVo<Integer> orderItemSwap(OrderItemSwapReq req);
    /**
     * 修改订单状态
     * @param orderId
     * @param status
     */
    public void updTaoOrderStatus(Long orderId, Integer status);

    /**
     * 订单发货
     * @param orderId 订单号
     * @param company 快递公司
     * @param code 物流单号
     */
     void orderSend(Long orderId,String company,String code);

     /**
      * 修改退货物流
      * @param orderId
      * @param company
      * @param code
      */
     void editRefundLogisticsCode(Long refundId,String company,String code,String address);
     ResultVo<String> signRefund(Long refundId,Integer auditStatus,String remark);
     /**
      * excel订单导入
      * @param orderList
      * @return
      */
     ResultVo<String> importExcelOrder(List<OrderImportPiPiEntity> orderList,Integer shopId);
}
