package com.b2c.interfaces.tao;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.tao.TaokeywordViewEntity;

public interface TaokeywordViewService {
    
    Integer addKeyword(Integer shopId,String keyword, String source, Long goodsId, Integer views,String date);

    PagingResponse<TaokeywordViewEntity> getList(Integer shopId,Integer pageIndex, Integer pageSize, Long goodsId, String keyword,String date);
}
