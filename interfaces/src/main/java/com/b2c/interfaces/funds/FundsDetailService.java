package com.b2c.interfaces.funds;

import java.util.List;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.FundsDetailEntity;
import com.b2c.entity.funds.LedgerDayModel;
import com.b2c.entity.result.ResultVo;

public interface FundsDetailService {
    PagingResponse<FundsDetailEntity> getFundsDetailList(Integer pageIndex, Integer pageSize, Integer type, String source, String sourceNo, String createDate);
    ResultVo<Long> create(FundsDetailEntity fEntity);

    /**
     * 获取日总账
     * @param startDate
     * @param endDate
     * @return
     */
    List<LedgerDayModel> getLedgerDayReport(String startDate,String endDate);
}
