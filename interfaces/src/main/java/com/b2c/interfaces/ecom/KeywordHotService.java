package com.b2c.interfaces.ecom;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.bd.KeywordHotEntity;

public interface KeywordHotService {
    PagingResponse<KeywordHotEntity> getList(Integer pageIndex, Integer pageSize,String keyword,String platform,String category);
    void add(KeywordHotEntity entity);
}
