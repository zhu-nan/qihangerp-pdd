package com.b2c.interfaces.ecom;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ecom.KeywordDataEntity;
import com.b2c.entity.ecom.KeywordsEntity;

public interface EcomKeywordService {
    PagingResponse<KeywordsEntity> getList(Integer pageIndex, Integer pageSize,String category,String category2,String platform,String source,String keyword);

    PagingResponse<KeywordDataEntity> getDataList(Integer pageIndex, Integer pageSize,Long keywordId);
    void addKeyWord(KeywordsEntity entity,KeywordDataEntity dataEntity);
    void addKeywordData(KeywordDataEntity entity);
    KeywordsEntity getKeywordById(Long keywordId);
}
