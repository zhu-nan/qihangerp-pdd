package com.b2c.interfaces.ecom;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ecom.GoodsStyleDataEntity;
import com.b2c.entity.ecom.GoodsStyleReferEntity;

public interface GoodsStyleReferService {
    PagingResponse<GoodsStyleReferEntity> getList(Integer pageIndex, Integer pageSize,String platform,String type);
    void add(GoodsStyleReferEntity entity);
}
