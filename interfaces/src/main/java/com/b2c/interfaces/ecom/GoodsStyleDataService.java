package com.b2c.interfaces.ecom;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ecom.GoodsStyleDataEntity;

public interface GoodsStyleDataService {
    PagingResponse<GoodsStyleDataEntity> getList(Integer pageIndex, Integer pageSize,String num,String platform);
    void add(GoodsStyleDataEntity entity);
}
