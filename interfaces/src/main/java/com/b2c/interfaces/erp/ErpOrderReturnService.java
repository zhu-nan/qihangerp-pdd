package com.b2c.interfaces.erp;

import com.b2c.entity.result.PagingResponse;
import com.b2c.entity.ErpOrderReturnEntity;
import com.b2c.entity.result.ResultVo;
import com.b2c.entity.vo.ErpOrderReturnItemVo;
import com.b2c.entity.vo.ErpOrderReturnVo;

import java.util.List;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-10-15 15:15
 */
public interface ErpOrderReturnService {
    /**
     * 搜索退货单
     * @param pageIndex
     * @param pageSize
     * @param refNum 退货单号
     * @param orderNum 订单号
     * @param logisticsCode 买家发货物流单号
     * @param refMobile 退货人手机号
     * @return
     */
    PagingResponse<ErpOrderReturnVo> getList(int pageIndex, int pageSize, String refNum,String orderNum,String logisticsCode,String refMobile,Integer status);
    List<ErpOrderReturnItemVo> getItemListById(Long returnOrderId);
    ErpOrderReturnEntity getById(Long returnOrderId);
    /**
     * 确认收货 并入库
     * @param returnOrderId
     * @return
     */
    ResultVo<Integer> confirmReceiveAndStockIn(Long returnOrderId,String[] returnItemIds,String[] locationIds,Integer userId, String trueName);
    /**
     * 合格检验列表
     * @param id
     * @return
     */
    public ErpOrderReturnVo getStockInfo(Long id);
    /**
     * 检查仓位是否存在
     * @param specNumber
     * @param locationNmae
     * @return
     */
    public Integer checkStockInfo(String specNumber ,String locationNmae);
    public void erp_order_return_cancel(Long erpOrderReturnId);

    /**
     * 保存合格检验单
     * @param locationIds 仓库id
     * @param goodsIds
     * @param specIds 规格id
     * @param quantities
     */
//    public Integer returnOrderStockIn(String[] locationIds,String[] goodsIds ,String[] specIds ,String[] quantities , String[] itemIds, Integer userId ,String userName ,String billNo ,Long returnId);

    /**
     * 检查不良品仓位
     * @param specNumber
     * @param locationNmae
     * @return
     */
    //public Integer checkBadStock(String specNumber, String locationNmae);

    /**
     * 保存不良品检验单
     * @param goodsIds
     * @param specNumbers
     * @param quantities
     */
     Integer returnOrderStockBadIn(String[] goodsIds ,String[] specNumbers ,String[] quantities ,String[] itemIds ,String billNo ,Integer returnId);

    PagingResponse<ErpOrderReturnItemVo> getReturnCompleteItemList(Integer pageIndex,Integer pageSize,String orderNum,String skuNumber,Integer startTime,Integer endTime);

    List<ErpOrderReturnItemVo> getReturnCompleteItemListForExcel(String orderNum,String skuNumber,Integer startTime,Integer endTime);


}
