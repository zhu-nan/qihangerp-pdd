package com.b2c.interfaces;

import com.b2c.entity.ShopRefundOrderEntity;
import com.b2c.entity.result.ResultVo;

import java.util.List;

/**
 * 描述：网店退货单service
 *
 * @author qlp
 * @date 2021-06-21 17:55
 */
public interface ShopRefundOrderService {
    /**
     * 获取网店退货单信息
     * @param logisticsCode 物流编号
     * @param shopType 店铺类型（5拼多多4淘宝天猫）
     * @return
     */
    ResultVo<List<ShopRefundOrderEntity>> getShopRefundOrder(String logisticsCode, Integer shopType);

    /**
     * 退货签收
     * @param refundId
     * @param shopType
     * @return
     */
    ResultVo<Integer> refundReceiveConfirm(Long refundId, Integer shopType);

    /**
     * 退货处理标记
     * @param refundId
     * @param shopType
     * @param status
     * @param remark
     * @return
     */
    ResultVo<Integer> signRefund(Long refundId, Integer shopType,Integer status,String remark);

    /**
     * 退货标记备注
     * @param refundId
     * @param shopType
     * @param remark
     * @return
     */
    ResultVo<Integer> signRemark(Long refundId,Integer shopType,String remark);
}
