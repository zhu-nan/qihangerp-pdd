package com.b2c.interfaces;

import java.util.List;

import com.b2c.entity.vo.GoodsSalesAnalyseVo;
import com.b2c.entity.vo.GoodsSpecSalesAnalyseVo;
import com.b2c.entity.result.PagingResponse;

/**
 * 商品销售分析
 */
public interface GoodsSalesAnalyseService {
    PagingResponse<GoodsSalesAnalyseVo> getGoodsSalesReport(Integer pageIndex, Integer pageSize, Integer shopId, String goodsNum);
    List<GoodsSpecSalesAnalyseVo> getGoodsSpecSalesReport(Integer goodsId);
    List<GoodsSalesAnalyseVo> getGoodsSalesReportPdd(Integer shopId);
}
