package com.b2c.entity;

import java.math.BigDecimal;

public class KeyWordEntity {
    private Long id;
    private Long parentId;
    private String category;
    private String categoryName;
    private String source;
    private String keyword;
    private Integer sousuorenqi;
    private Integer sousuoredu;
    private Integer dianjirenqi;
    private Integer dianjiredu;
    private BigDecimal dianjilv;
    private BigDecimal zhifulv;
    private String includeYear;
    private String includeMonth;
    private String includeDate;
    private String createTime;
    private Integer depth;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Integer getDepth() {
        return depth;
    }

    public void setDepth(Integer depth) {
        this.depth = depth;
    }

    public Integer getSousuoredu() {
        return sousuoredu;
    }

    public void setSousuoredu(Integer sousuoredu) {
        this.sousuoredu = sousuoredu;
    }

    public Integer getDianjiredu() {
        return dianjiredu;
    }

    public void setDianjiredu(Integer dianjiredu) {
        this.dianjiredu = dianjiredu;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Integer getSousuorenqi() {
        return sousuorenqi;
    }

    public void setSousuorenqi(Integer sousuorenqi) {
        this.sousuorenqi = sousuorenqi;
    }

    public Integer getDianjirenqi() {
        return dianjirenqi;
    }

    public void setDianjirenqi(Integer dianjirenqi) {
        this.dianjirenqi = dianjirenqi;
    }

    public BigDecimal getDianjilv() {
        return dianjilv;
    }

    public void setDianjilv(BigDecimal dianjilv) {
        this.dianjilv = dianjilv;
    }

    public BigDecimal getZhifulv() {
        return zhifulv;
    }

    public void setZhifulv(BigDecimal zhifulv) {
        this.zhifulv = zhifulv;
    }

    public String getIncludeYear() {
        return includeYear;
    }

    public void setIncludeYear(String includeYear) {
        this.includeYear = includeYear;
    }

    public String getIncludeMonth() {
        return includeMonth;
    }

    public void setIncludeMonth(String includeMonth) {
        this.includeMonth = includeMonth;
    }

    public String getIncludeDate() {
        return includeDate;
    }

    public void setIncludeDate(String includeDate) {
        this.includeDate = includeDate;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}
