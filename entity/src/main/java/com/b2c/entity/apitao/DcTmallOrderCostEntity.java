package com.b2c.entity.apitao;

public class DcTmallOrderCostEntity {
    /**
     * id
     */
    private Long id;

    /**
     * 订单id
     */
    private String dcTmallOrderId;

    /**
     * 类型id
     */
    private Integer tmallOrderTagId;
    /**
     * 备注
     */
    private String remark;
    /**
     * 金额
     */
    private Double amount;

    private String tagName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDcTmallOrderId() {
        return dcTmallOrderId;
    }

    public void setDcTmallOrderId(String dcTmallOrderId) {
        this.dcTmallOrderId = dcTmallOrderId;
    }

    public Integer getTmallOrderTagId() {
        return tmallOrderTagId;
    }

    public void setTmallOrderTagId(Integer tmallOrderTagId) {
        this.tmallOrderTagId = tmallOrderTagId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
