package com.b2c.entity.apitao;

import java.math.BigDecimal;
import java.util.List;

public class PddOrderStatisVo {
    private Integer orderCount;
    private BigDecimal totalPrice;
    private List<PddOrderStatis> items;

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public List<PddOrderStatis> getItems() {
        return items;
    }

    public void setItems(List<PddOrderStatis> items) {
        this.items = items;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }
}
