package com.b2c.entity.funds;

public class OrderSettlementPddEntity {
    private Long id;
    private String orderSn;
    private String settlementTime;
    private Double income;
    private Double expend;
    private String type;
    private String remark;
    private String description;
    private String createTime;
    private String modifyTime;
    private Integer shopId;

    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getOrderSn() {
        return orderSn;
    }
    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }
    public String getSettlementTime() {
        return settlementTime;
    }
    public void setSettlementTime(String settlementTime) {
        this.settlementTime = settlementTime;
    }
    public Double getIncome() {
        return income;
    }
    public void setIncome(Double income) {
        this.income = income;
    }
    public Double getExpend() {
        return expend;
    }
    public void setExpend(Double expend) {
        this.expend = expend;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getRemark() {
        return remark;
    }
    public void setRemark(String remark) {
        this.remark = remark;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getCreateTime() {
        return createTime;
    }
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
    public String getModifyTime() {
        return modifyTime;
    }
    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }
    public Integer getShopId() {
        return shopId;
    }
    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    
}
