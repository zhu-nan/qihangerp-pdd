package com.b2c.entity.query;

import java.io.Serializable;

/**
 * 退货入库明细查询
 */
public class ErpStockInItemQuery implements Serializable {
    //开始页码
    private Integer pageIndex;
    //页数
    private Integer pageSize;
    //入库单号
    private String billNo;
    //合同号
    private String contractNo;
    //入库编号
    private String stockInNumber;
    //sku编码
    private String skuNumber;
    //入库类型1采购入库2退货入库3盘点入库
    private Integer inType;
    //商品编码
    private String goodsNumber;
    //开始时间
    private Integer startTime;
    //结束时间
    private Integer endTime;

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getStartTime() {
        return startTime;
    }

    public void setStartTime(Integer startTime) {
        this.startTime = startTime;
    }

    public Integer getEndTime() {
        return endTime;
    }

    public void setEndTime(Integer endTime) {
        this.endTime = endTime;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getStockInNumber() {
        return stockInNumber;
    }

    public void setStockInNumber(String stockInNumber) {
        this.stockInNumber = stockInNumber;
    }

    public String getSkuNumber() {
        return skuNumber;
    }

    public void setSkuNumber(String skuNumber) {
        this.skuNumber = skuNumber;
    }

    public Integer getInType() {
        return inType;
    }

    public void setInType(Integer inType) {
        this.inType = inType;
    }

    public String getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(String goodsNumber) {
        this.goodsNumber = goodsNumber;
    }
}
