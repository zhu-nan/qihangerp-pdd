package com.b2c.entity.purchase;

public class BuHuoTiXingEntity {
    private Integer erpGoodsSpecId;
    private String productPic;
    private String colorImage;
    private String goodsNumber;
    private String code;
    private String specDesc;
    private Integer total;
    private Integer qty;

    public Integer getErpGoodsSpecId() {
        return erpGoodsSpecId;
    }

    public void setErpGoodsSpecId(Integer erpGoodsSpecId) {
        this.erpGoodsSpecId = erpGoodsSpecId;
    }

    public String getColorImage() {
        return colorImage;
    }

    public void setColorImage(String colorImage) {
        this.colorImage = colorImage;
    }

    public String getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(String goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public String getSpecDesc() {
        return specDesc;
    }

    public void setSpecDesc(String specDesc) {
        this.specDesc = specDesc;
    }

    public String getProductPic() {
        return productPic;
    }

    public void setProductPic(String productPic) {
        this.productPic = productPic;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }
}
