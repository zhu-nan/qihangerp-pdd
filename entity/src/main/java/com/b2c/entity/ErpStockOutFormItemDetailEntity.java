package com.b2c.entity;

public class ErpStockOutFormItemDetailEntity {
    private Long id;
    private Long stockOutFormItemId;
    private Long goodsStockInfoId;
    private Long goodsStockInfoItemId;
    private Long quantity;
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Long getStockOutFormItemId() {
        return stockOutFormItemId;
    }
    public void setStockOutFormItemId(Long stockOutFormItemId) {
        this.stockOutFormItemId = stockOutFormItemId;
    }
    public Long getGoodsStockInfoId() {
        return goodsStockInfoId;
    }
    public void setGoodsStockInfoId(Long goodsStockInfoId) {
        this.goodsStockInfoId = goodsStockInfoId;
    }
    public Long getGoodsStockInfoItemId() {
        return goodsStockInfoItemId;
    }
    public void setGoodsStockInfoItemId(Long goodsStockInfoItemId) {
        this.goodsStockInfoItemId = goodsStockInfoItemId;
    }
    public Long getQuantity() {
        return quantity;
    }
    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    
    
}
