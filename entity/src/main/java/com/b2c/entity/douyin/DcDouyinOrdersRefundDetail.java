package com.b2c.entity.douyin;
/**
 * 退货单子表
 */
public class DcDouyinOrdersRefundDetail {
    private ApplyInfo applyInfo;
    private ProcessBar processBar;
    private StageInfo stageInfo;

    public StageInfo getStageInfo() {
        return stageInfo;
    }

    public void setStageInfo(StageInfo stageInfo) {
        this.stageInfo = stageInfo;
    }

    public ApplyInfo getApplyInfo() {
        return applyInfo;
    }

    public void setApplyInfo(ApplyInfo applyInfo) {
        this.applyInfo = applyInfo;
    }

    public ProcessBar getProcessBar() {
        return processBar;
    }

    public void setProcessBar(ProcessBar processBar) {
        this.processBar = processBar;
    }

    public static class ApplyInfo{
        private String questionDesc;
        private int comboAmount;
        private String buyerName;
        private String productName;
        private String specs;
        private String orderCeateTime;
        private String userImgs;
        private int totalAmount;
        private String buyerTel;
        private String productId;
        private String mainImg;
        private String except;
        private String orderId;

        public String getQuestionDesc() {
            return questionDesc;
        }

        public void setQuestionDesc(String questionDesc) {
            this.questionDesc = questionDesc;
        }

        public int getComboAmount() {
            return comboAmount;
        }

        public void setComboAmount(int comboAmount) {
            this.comboAmount = comboAmount;
        }

        public String getBuyerName() {
            return buyerName;
        }

        public void setBuyerName(String buyerName) {
            this.buyerName = buyerName;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getSpecs() {
            return specs;
        }

        public void setSpecs(String specs) {
            this.specs = specs;
        }

        public String getOrderCeateTime() {
            return orderCeateTime;
        }

        public void setOrderCeateTime(String orderCeateTime) {
            this.orderCeateTime = orderCeateTime;
        }

        public String getUserImgs() {
            return userImgs;
        }

        public void setUserImgs(String userImgs) {
            this.userImgs = userImgs;
        }

        public int getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(int totalAmount) {
            this.totalAmount = totalAmount;
        }

        public String getBuyerTel() {
            return buyerTel;
        }

        public void setBuyerTel(String buyerTel) {
            this.buyerTel = buyerTel;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getMainImg() {
            return mainImg;
        }

        public void setMainImg(String mainImg) {
            this.mainImg = mainImg;
        }

        public String getExcept() {
            return except;
        }

        public void setExcept(String except) {
            this.except = except;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }
    }
    public static class StageInfo{
        private logisticsInfo logisticsInfo;

        public StageInfo.logisticsInfo getLogisticsInfo() {
            return logisticsInfo;
        }

        public void setLogisticsInfo(StageInfo.logisticsInfo logisticsInfo) {
            this.logisticsInfo = logisticsInfo;
        }
        public static class logisticsInfo{
            private String logisticsName;
            private String logisticsCode;

            public String getLogisticsName() {
                return logisticsName;
            }

            public void setLogisticsName(String logisticsName) {
                this.logisticsName = logisticsName;
            }

            public String getLogisticsCode() {
                return logisticsCode;
            }

            public void setLogisticsCode(String logisticsCode) {
                this.logisticsCode = logisticsCode;
            }
        }
    }
    public static class ProcessBar{
        private String  applyTime;

        public String getApplyTime() {
            return applyTime;
        }
        public void setApplyTime(String applyTime) {
            this.applyTime = applyTime;
        }
    }
}
