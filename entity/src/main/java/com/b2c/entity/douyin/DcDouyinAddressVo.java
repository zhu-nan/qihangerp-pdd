package com.b2c.entity.douyin;

public class DcDouyinAddressVo {
    private Province province;
    private Town town;
    private City city;
    private Street street;
    private String detail;
    private String encryptDetail;
    public void setProvince(Province province) {
        this.province = province;
    }
    public  Province getProvince() {
        return province;
    }

    public void setTown(Town town) {
        this.town = town;
    }
    public Town getTown() {
        return town;
    }

    public void setCity(City city) {
        this.city = city;
    }
    public City getCity() {
        return city;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
    public String getDetail() {
        return detail;
    }

    public static class Province {

        private String name;
        public void setName(String name) {
            this.name = name;
        }
        public String getName() {
            return name;
        }


    }


    public static class Town {

        private String name;
        public void setName(String name) {
            this.name = name;
        }
        public String getName() {
            return name;
        }


    }
    public static class City {

        private String name;
        public void setName(String name) {
            this.name = name;
        }
        public String getName() {
            return name;
        }


    }

    public static class Street {

        private String name;
        public void setName(String name) {
            this.name = name;
        }
        public String getName() {
            return name;
        }


    }

    public String getEncryptDetail() {
        return encryptDetail;
    }

    public void setEncryptDetail(String encryptDetail) {
        this.encryptDetail = encryptDetail;
    }

    public Street getStreet() {
        return street;
    }

    public void setStreet(Street street) {
        this.street = street;
    }
}
