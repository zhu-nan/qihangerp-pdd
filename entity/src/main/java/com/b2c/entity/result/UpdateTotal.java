package com.b2c.entity.result;

/**
 * 描述：
 * 更新的数量
 *
 * @author qlp
 * @date 2019-04-15 15:59
 */
public class UpdateTotal {
    private int updateCount;//更新数量
    private int insertCount;//新增数量

    public UpdateTotal(int updateCount, int insertCount) {
        this.updateCount = updateCount;
        this.insertCount = insertCount;
    }

    public int getUpdateCount() {
        return updateCount;
    }

    public void setUpdateCount(int updateCount) {
        this.updateCount = updateCount;
    }

    public int getInsertCount() {
        return insertCount;
    }

    public void setInsertCount(int insertCount) {
        this.insertCount = insertCount;
    }
}
