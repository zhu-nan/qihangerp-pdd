package com.b2c.entity.enums;

public enum EnumPddLogistics {
    ST("申通快递", 1),
    BS("百世快递", 3),
    SF("顺丰快递", 44),
    YT("圆通快递", 85),
    ZT("中通快递", 115),
    YD("韵达快递",121),
    JT("极兔快递",384);

    private String name;
    private int index;

    // 构造方法
    private EnumPddLogistics(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (EnumPddLogistics c : EnumPddLogistics.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return "";
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
