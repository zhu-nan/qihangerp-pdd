package com.b2c.entity.enums.erp;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * 描述：
 * 退货订单状态
 *
 * @author qlp
 * @date 2019-02-21 13:44
 */
public enum EnumErpSalesOrderRefundStatus {
    //退款状态 -1取消申请；0拒绝退货；1申请中(待审核)；2等待买家发货；3买家已发货(待收货)；4已收货（完成）
    Applying("待审核", 1),
    Agree("等待买家发货", 2),//待买家发货
    Delivered("买家已发货", 3),//买家已发货，待收货
    Received("已收货", 4),//已收货
    Success("退货成功", 5),
    Refuse("拒绝退货", 0),
    Cancel("取消申请", -1);

    // 成员变量
    private String name;
    private int index;

    // 构造方法
    private EnumErpSalesOrderRefundStatus(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (EnumErpSalesOrderRefundStatus c : EnumErpSalesOrderRefundStatus.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    /**
     * 将该枚举全部转化成json
     *
     * @return
     */
    public static String toJson() {
        JSONArray jsonArray = new JSONArray();
        for (EnumErpSalesOrderRefundStatus e : EnumErpSalesOrderRefundStatus.values()) {
            JSONObject object = new JSONObject();
            object.put("name", e.getName());
            object.put("value", e.getIndex());
            jsonArray.add(object);
        }
        return jsonArray.toString();
    }
}
