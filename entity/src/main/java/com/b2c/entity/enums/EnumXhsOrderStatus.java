package com.b2c.entity.enums;

public enum EnumXhsOrderStatus {
    //小红书订单状态，1已下单待付款 2已支付处理中 3清关中 4待发货 5部分发货 6待收货 7已完成 8已关闭 9已取消 10换货申请中
    STATUS_1("1已下单待付款",  1),
    STATUS_2("2已支付处理中",  2),
    STATUS_3("3清关中", 3),
    STATUS_4("4待发货", 4),
    STATUS_5("5部分发货", 5),
    STATUS_6("6待收货", 6),
    STATUS_7("7已完成", 7),
    STATUS_8("8已关闭", 8),
    STATUS_9("9已取消", 9),
    STATUS_10("10换货申请中", 10);

    // 成员变量
    private String name;
    private Integer index;

    // 构造方法
    private EnumXhsOrderStatus(String name, Integer index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getStatusName(Integer index) {
        for (EnumXhsOrderStatus c : EnumXhsOrderStatus.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }
}
