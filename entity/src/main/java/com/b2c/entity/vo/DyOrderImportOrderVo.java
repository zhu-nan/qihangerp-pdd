package com.b2c.entity.vo;

public class DyOrderImportOrderVo {

    private String orderId;


    //订单内容
    String payTypeName;
    String province;
    String city;
    String town;
    String street;
    String buyerWords;
    String orderCreateTime;
    String sellerWords;
    String orderPayTime;
    String appSource;
    String trafficeSource;
    String orderStatusStr;
    String expShipTime;
    String authorId;
    String authorName;

    //订单商品信息
    String subOrderId;
    String productName;
    String specDesc;
    Double quantity;
    String productId;
    String specCode;
    Double price;
    Double totalAmount;
    Double postAmount;
    Double couponAmount;
    public String getOrderId() {
        return orderId;
    }
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
    public String getPayTypeName() {
        return payTypeName;
    }
    public void setPayTypeName(String payTypeName) {
        this.payTypeName = payTypeName;
    }
    public String getProvince() {
        return province;
    }
    public void setProvince(String province) {
        this.province = province;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getTown() {
        return town;
    }
    public void setTown(String town) {
        this.town = town;
    }
    public String getStreet() {
        return street;
    }
    public void setStreet(String street) {
        this.street = street;
    }
    public String getBuyerWords() {
        return buyerWords;
    }
    public void setBuyerWords(String buyerWords) {
        this.buyerWords = buyerWords;
    }
    public String getOrderCreateTime() {
        return orderCreateTime;
    }
    public void setOrderCreateTime(String orderCreateTime) {
        this.orderCreateTime = orderCreateTime;
    }
    public String getSellerWords() {
        return sellerWords;
    }
    public void setSellerWords(String sellerWords) {
        this.sellerWords = sellerWords;
    }
    public String getOrderPayTime() {
        return orderPayTime;
    }
    public void setOrderPayTime(String orderPayTime) {
        this.orderPayTime = orderPayTime;
    }
    public String getAppSource() {
        return appSource;
    }
    public void setAppSource(String appSource) {
        this.appSource = appSource;
    }
    public String getTrafficeSource() {
        return trafficeSource;
    }
    public void setTrafficeSource(String trafficeSource) {
        this.trafficeSource = trafficeSource;
    }

    public String getExpShipTime() {
        return expShipTime;
    }
    public void setExpShipTime(String expShipTime) {
        this.expShipTime = expShipTime;
    }
    public String getAuthorId() {
        return authorId;
    }
    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }
    public String getAuthorName() {
        return authorName;
    }
    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }
    public String getSubOrderId() {
        return subOrderId;
    }
    public void setSubOrderId(String subOrderId) {
        this.subOrderId = subOrderId;
    }
    public String getProductName() {
        return productName;
    }
    public void setProductName(String productName) {
        this.productName = productName;
    }
    public String getSpecDesc() {
        return specDesc;
    }
    public void setSpecDesc(String specDesc) {
        this.specDesc = specDesc;
    }
    public Double getQuantity() {
        return quantity;
    }
    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }
    public String getProductId() {
        return productId;
    }
    public void setProductId(String productId) {
        this.productId = productId;
    }
    public String getSpecCode() {
        return specCode;
    }
    public void setSpecCode(String specCode) {
        this.specCode = specCode;
    }
    public Double getPrice() {
        return price;
    }
    public void setPrice(Double price) {
        this.price = price;
    }
    public Double getTotalAmount() {
        return totalAmount;
    }
    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }
    public Double getPostAmount() {
        return postAmount;
    }
    public void setPostAmount(Double postAmount) {
        this.postAmount = postAmount;
    }
    public Double getCouponAmount() {
        return couponAmount;
    }
    public void setCouponAmount(Double couponAmount) {
        this.couponAmount = couponAmount;
    }
    public String getOrderStatusStr() {
        return orderStatusStr;
    }
    public void setOrderStatusStr(String orderStatusStr) {
        this.orderStatusStr = orderStatusStr;
    }

    
}
