package com.b2c.entity.vo;


import com.b2c.entity.ErpStockOutFormEntity;
import com.b2c.entity.ErpStockOutFormItemEntity;

import java.util.List;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-10-09 14:55
 */
public class ErpStockOutFormDetailVo extends ErpStockOutFormEntity {

    

    private List<ErpStockOutFormItemEntity> items;

    public List<ErpStockOutFormItemEntity> getItems() {
        return items;
    }

    public void setItems(List<ErpStockOutFormItemEntity> items) {
        this.items = items;
    }
}
