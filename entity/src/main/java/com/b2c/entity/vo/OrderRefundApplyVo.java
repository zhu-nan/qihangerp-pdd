package com.b2c.entity.vo;

import java.util.List;

public class OrderRefundApplyVo {
    private Long orderId;//申请退货的订单id
    private List<OrderRefundApplyItemVo> returnItems;//退货数据

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public List<OrderRefundApplyItemVo> getReturnItems() {
        return returnItems;
    }

    public void setReturnItems(List<OrderRefundApplyItemVo> returnItems) {
        this.returnItems = returnItems;
    }
}
