package com.b2c.entity.vo;

import com.b2c.entity.ErpStockInFormEntity;
import com.b2c.entity.ErpStockInFormItemEntity;

import java.util.List;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-10-09 14:55
 */
public class ErpStockInFormDetailVo extends ErpStockInFormEntity {
    private List<ErpStockInFormItemEntity> items;

    public List<ErpStockInFormItemEntity> getItems() {
        return items;
    }

    public void setItems(List<ErpStockInFormItemEntity> items) {
        this.items = items;
    }
}
