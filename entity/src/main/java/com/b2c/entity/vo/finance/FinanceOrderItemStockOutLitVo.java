package com.b2c.entity.vo.finance;

/**
 * 订单明细出库记录
 */
public class FinanceOrderItemStockOutLitVo {
    /**商品基本信息**/
    private String goodsName;
    private String goodsNumber;
    private String specName;
    private String specNumber;
    private Long quantity;

    /**出库信息**/
    private String stockOutNo;//出库单号
    private Long stockOutTime;//出库完成时间

    /**出库仓位信息**/
    private String locationNumber;//出库仓位

    /**出库订单信息**/
    private Long orderId;
    private Long itemId;
    private String orderNum;//对应订单号
    private String orderStatus;
    private Integer shopId;//对应店铺id
    private String shopName;//店铺名

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(String goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public String getSpecName() {
        return specName;
    }

    public void setSpecName(String specName) {
        this.specName = specName;
    }

    public String getSpecNumber() {
        return specNumber;
    }

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public String getStockOutNo() {
        return stockOutNo;
    }

    public void setStockOutNo(String stockOutNo) {
        this.stockOutNo = stockOutNo;
    }

    public Long getStockOutTime() {
        return stockOutTime;
    }

    public void setStockOutTime(Long stockOutTime) {
        this.stockOutTime = stockOutTime;
    }

    public String getLocationNumber() {
        return locationNumber;
    }

    public void setLocationNumber(String locationNumber) {
        this.locationNumber = locationNumber;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }
}
