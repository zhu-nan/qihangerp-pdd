package com.b2c.entity.vo;

import com.b2c.entity.ErpStockInCheckoutItemEntity;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-09-29 15:31
 */
public class ErpStockInCheckoutItemVo extends ErpStockInCheckoutItemEntity {
    private String goodsName;
    private String colorValue;
    private String sizeValue;

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getColorValue() {
        return colorValue;
    }

    public void setColorValue(String colorValue) {
        this.colorValue = colorValue;
    }

    public String getSizeValue() {
        return sizeValue;
    }

    public void setSizeValue(String sizeValue) {
        this.sizeValue = sizeValue;
    }
}
