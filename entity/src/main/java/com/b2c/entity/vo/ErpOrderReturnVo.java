package com.b2c.entity.vo;

import com.b2c.entity.ErpGoodsStockInfoEntity;
import com.b2c.entity.ErpOrderReturnEntity;

import java.util.List;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-10-15 15:00
 */
public class ErpOrderReturnVo extends ErpOrderReturnEntity {
    private List<ErpGoodsStockInfoEntity> infos;
    private List<ErpOrderReturnItemVo>  items;

    public List<ErpOrderReturnItemVo> getItems() {
        return items;
    }

    public void setItems(List<ErpOrderReturnItemVo> items) {
        this.items = items;
    }

    public List<ErpGoodsStockInfoEntity> getInfos() {
        return infos;
    }

    public void setInfos(List<ErpGoodsStockInfoEntity> infos) {
        this.infos = infos;
    }
}
