package com.b2c.entity.vo;

import com.b2c.entity.ErpGoodsStockLogsEntity;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-10-08 17:11
 */
public class ErpGoodsStockLogsListVo extends ErpGoodsStockLogsEntity {
    private String goodsName;
    private String colorValue;
    private String sizeValue;
    private String locationNumber;

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getColorValue() {
        return colorValue;
    }

    public void setColorValue(String colorValue) {
        this.colorValue = colorValue;
    }

    public String getSizeValue() {
        return sizeValue;
    }

    public void setSizeValue(String sizeValue) {
        this.sizeValue = sizeValue;
    }

    public String getLocationNumber() {
        return locationNumber;
    }

    public void setLocationNumber(String locationNumber) {
        this.locationNumber = locationNumber;
    }
}
