package com.b2c.entity.vo;

import java.util.List;

/**
 * 描述：
 * 待发货订单List
 *
 * @author qlp
 * @date 2019-06-20 15:46
 */
public class OrderScanCodeVo {
    private Long id;//订单id
    private String sn;//订单号
    private Long t;//出库时间
    private String k;
    private String ad;//收货地址
    private Integer st;//状态
    private String s;//店铺名

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public Long getT() {
        return t;
    }

    public void setT(Long t) {
        this.t = t;
    }

    public String getK() {
        return k;
    }

    public void setK(String k) {
        this.k = k;
    }

    public String getAd() {
        return ad;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    public Integer getSt() {
        return st;
    }

    public void setSt(Integer st) {
        this.st = st;
    }

    public String getS() {
        return s;
    }

    public void setS(String s) {
        this.s = s;
    }

    private List<OrderItemScanCodeVo> items;  //订单商品列表

    public List<OrderItemScanCodeVo> getItems() {
        return items;
    }

    public void setItems(List<OrderItemScanCodeVo> items) {
        this.items = items;
    }
}
