package com.b2c.entity.vo;

import com.b2c.entity.OrderLogisticsEntity;
import com.b2c.entity.OrderLogsEntity;
import com.b2c.entity.mall.OrderCancelItemEntity;

import java.util.List;

/**
 * 描述：
 * 退货订单详情
 *
 * @author qlp
 * @date 2019-02-26 17:00
 */
public class RefundOrderDetailVo extends RefundOrderListVo {
    private List<OrderLogsEntity> logs;  //订单日志
    private List<OrderLogisticsEntity> logistics;//物流信息
    private List<OrderCancelItemEntity> cancelItems;

    public List<OrderCancelItemEntity> getCancelItems() {
        return cancelItems;
    }

    public void setCancelItems(List<OrderCancelItemEntity> cancelItems) {
        this.cancelItems = cancelItems;
    }

    public List<OrderLogisticsEntity> getLogistics() {
        return logistics;
    }

    public void setLogistics(List<OrderLogisticsEntity> logistics) {
        this.logistics = logistics;
    }

    public List<OrderLogsEntity> getLogs() {
        return logs;
    }

    public void setLogs(List<OrderLogsEntity> logs) {
        this.logs = logs;
    }
}
