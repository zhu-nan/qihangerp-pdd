package com.b2c.entity.vo.finance;

import com.b2c.entity.ErpStockInFormItemEntity;

import java.math.BigDecimal;
import java.util.Date;

public class ErpStockInFormItemVo extends ErpStockInFormItemEntity {
    private Date stockInTime;//入库时间
    private Long stockInTime1;//入库时间（新版）
    private String billNo;//采购单号
    private String contractNo;//合同号
    private String contactName;//供应商
    private String billDate;//采购日期
    private String stockInNumber;//入库单
    private String stockInUserName;
    private String stockInId;//入库单ID
    private BigDecimal purPrice;//采购价
    private BigDecimal price;//采购价

    public String getStockInUserName() {
        return stockInUserName;
    }

    public void setStockInUserName(String stockInUserName) {
        this.stockInUserName = stockInUserName;
    }

    public Long getStockInTime1() {
        return stockInTime1;
    }

    public void setStockInTime1(Long stockInTime1) {
        this.stockInTime1 = stockInTime1;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getStockInId() {
        return stockInId;
    }

    public void setStockInId(String stockInId) {
        this.stockInId = stockInId;
    }

    public String getStockInNumber() {
        return stockInNumber;
    }

    public void setStockInNumber(String stockInNumber) {
        this.stockInNumber = stockInNumber;
    }

    public Date getStockInTime() {
        return stockInTime;
    }

    public void setStockInTime(Date stockInTime) {
        this.stockInTime = stockInTime;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    public BigDecimal getPurPrice() {
        return purPrice;
    }

    public void setPurPrice(BigDecimal purPrice) {
        this.purPrice = purPrice;
    }
}
