package com.b2c.entity.kwai;

public enum EnumKwaiExpressCodeVo {
    //订单状态0已取消1待支付2支付成功（待发货）3已发货（待收货）4已收货5已完成（过了售后期限）
    SF("顺丰速运", 4),
    ZT("中通快递", 9),
    YT("圆通速递", 6),//待发货
    ST("申通快递", 3),//待收货
    YD("韵达快递", 11),//已收货
    EMS("EMS", 2),//过了售后期限
    JD("京东", 14),//退款中
    TT("天天快递", 5),//交易失败
    ZJS("宅急送", 10),
    YZ("邮政快递包裹", 8),
    YS("优速物流", 15),
    DB("德邦", 17);//审核中

    // 成员变量
    private String name;
    private int index;

    // 构造方法
    private EnumKwaiExpressCodeVo(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (EnumKwaiExpressCodeVo c : EnumKwaiExpressCodeVo.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }
    public static int getIndex(String name) {
        for (EnumKwaiExpressCodeVo c : EnumKwaiExpressCodeVo.values()) {
            if (c.getName().equals(name)) {
                return c.index;
            }
        }
        return 0;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
