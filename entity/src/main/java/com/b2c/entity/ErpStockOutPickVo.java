package com.b2c.entity;

/**
 * 描述：
 * 商品拣货vo
 *
 * @author qlp
 * @date 2019-09-20 16:17
 */
public class ErpStockOutPickVo {
//    private Long orderItemId;
    private Long goodsId;
    private Long skuId;
    private Long quantity;
    private Integer locationId;
//
//    public Long getOrderItemId() {
//        return orderItemId;
//    }
//
//    public void setOrderItemId(Long orderItemId) {
//        this.orderItemId = orderItemId;
//    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }
}
