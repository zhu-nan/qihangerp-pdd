package com.b2c.entity.pdd;

public class ShopDataReportModel {
    private String date;

    private Integer fks=0;//访客数
    private Integer scs;//收藏数
    private Integer dds;//订单数
    private Integer fwsps;//访问商品数

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getFks() {
        return fks;
    }

    public void setFks(Integer fks) {
        this.fks = fks;
    }

    public Integer getScs() {
        return scs;
    }

    public void setScs(Integer scs) {
        this.scs = scs;
    }

    public Integer getDds() {
        return dds;
    }

    public void setDds(Integer dds) {
        this.dds = dds;
    }

    public Integer getFwsps() {
        return fwsps;
    }

    public void setFwsps(Integer fwsps) {
        this.fwsps = fwsps;
    }
}
