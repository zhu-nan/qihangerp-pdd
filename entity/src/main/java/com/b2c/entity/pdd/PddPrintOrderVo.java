package com.b2c.entity.pdd;

import java.util.List;

public class PddPrintOrderVo {
    private List<OrderViewModel> orders;
    private String remark;

    public List<OrderViewModel> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderViewModel> orders) {
        this.orders = orders;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
