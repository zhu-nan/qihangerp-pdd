package com.b2c.entity.pdd;

import java.math.BigDecimal;

public class FinanceMonthReportPddVo {
    private String month;
    private BigDecimal income;
    private BigDecimal expend;
    private BigDecimal goodsCost;
    private BigDecimal goodsRefund;//货物退款
    private BigDecimal marketingFee;//营销费用
    public String getMonth() {
        return month;
    }
    public void setMonth(String month) {
        this.month = month;
    }
    public BigDecimal getIncome() {
        return income;
    }
    public void setIncome(BigDecimal income) {
        this.income = income;
    }
    public BigDecimal getExpend() {
        return expend;
    }
    public void setExpend(BigDecimal expend) {
        this.expend = expend;
    }
    public BigDecimal getGoodsCost() {
        return goodsCost;
    }
    public void setGoodsCost(BigDecimal goodsCost) {
        this.goodsCost = goodsCost;
    }
    public BigDecimal getGoodsRefund() {
        return goodsRefund;
    }
    public void setGoodsRefund(BigDecimal goodsRefund) {
        this.goodsRefund = goodsRefund;
    }
    public BigDecimal getMarketingFee() {
        return marketingFee;
    }
    public void setMarketingFee(BigDecimal marketingFee) {
        this.marketingFee = marketingFee;
    }

    
}
