package com.b2c.entity.datacenter.vo;

import com.b2c.entity.OrderItemEntity;
import com.b2c.entity.OrdersEntity;

import java.util.List;

/**
 * 描述：
 *订单列表vo
 *
 * @author qlp
 * @date 2019-09-16 19:56
 */
public class OfflineOrderListVo extends OrdersEntity {
    private String addressName;
    private String address;
    private String myMobile;
    private Integer totalCount;//商品数量
    private List<OrderItemEntity> items;

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    @Override
    public String getAddress() {
        return address;
    }

    @Override
    public void setAddress(String address) {
        this.address = address;
    }

    public String getMyMobile() {
        return myMobile;
    }

    public void setMyMobile(String myMobile) {
        this.myMobile = myMobile;
    }

    public List<OrderItemEntity> getItems() {
        return items;
    }

    public void setItems(List<OrderItemEntity> items) {
        this.items = items;
    }

}
