package com.b2c.entity.datacenter;

/**
 *
 */
public class DcTmallOrderRefundEntity {

    private Long id;//` BIGINT(20) NOT NULL AUTO_INCREMENT,
    private String refund_id;//` VARCHAR(50) NULL DEFAULT NULL COMMENT '退款id',
    private Long tid;//` BIGINT(20) NULL DEFAULT NULL COMMENT '淘宝交易单号（订单号）',
    private Long oid;//` BIGINT(20) NULL DEFAULT NULL COMMENT '子订单号。如果是单笔交易oid会等于tid',
    private String buyer_nick;//` VARCHAR(50) NULL DEFAULT NULL COMMENT '买家昵称',
    private String total_fee;//` VARCHAR(10) NULL DEFAULT NULL COMMENT '交易总金额。精确到2位小数;单位:元。如:200.07，表示:200元7分',
    private String payment;//支付给卖家的金额(交易总金额-退还给买家的金额)。精确到2位小数;单位:元。如:200.07，表示:200元7分
    private String refund_fee;//` VARCHAR(10) NULL DEFAULT NULL COMMENT '退还金额(退还给买家的金额)。精确到2位小数;单位:元。如:200.07，表示:200元7分',
    private Long created;//` BIGINT(20) NULL DEFAULT NULL COMMENT '退款申请时间。',
    private Long modified;//` BIGINT(20) NULL DEFAULT NULL COMMENT '更新时间。',
    private String order_status;//` VARCHAR(50) NULL DEFAULT NULL COMMENT '退款对应的订单交易状态。',
    private String status;//` VARCHAR(50) NULL DEFAULT NULL COMMENT '退款状态。可选值WAIT_SELLER_AGREE(买家已经申请退款，等待卖家同意) WAIT_BUYER_RETURN_GOODS(卖家已经同意退款，等待买家退货) WAIT_SELLER_CONFIRM_GOODS(买家已经退货，等待卖家确认收货) SELLER_REFUSE_BUYER(卖家拒绝退款) CLOSED(退款关闭) SUCCESS(退款成功)',
    private String good_status;//` VARCHAR(50) NULL DEFAULT NULL COMMENT '货物状态。可选值BUYER_NOT_RECEIVED (买家未收到货) BUYER_RECEIVED (买家已收到货) BUYER_RETURNED_GOODS (买家已退货)',
    private Integer has_good_return;//` INT(11) NULL DEFAULT NULL COMMENT '买家是否需要退货。可选值:true(是),false(否)',
    private String reason;//` VARCHAR(100) NULL DEFAULT NULL COMMENT '退款原因',
    private String desc;//` VARCHAR(100) NULL DEFAULT NULL COMMENT '退款说明',
    private String logisticsCompany;//` VARCHAR(50) NULL DEFAULT NULL COMMENT '物流公司',
    private String logisticsCode;//` VARCHAR(50) NULL DEFAULT NULL COMMENT '物流单号',
    private Integer auditStatus;//订单审核状态（0待审核1已审核）
    private Integer shopId;//店铺Id
    private String refundPhase;//退款阶段，可选值：onsale/aftersale
    private Long num;//退货数量

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getRefundPhase() {
        return refundPhase;
    }

    public void setRefundPhase(String refundPhase) {
        this.refundPhase = refundPhase;
    }

    public Long getNum() {
        return num;
    }

    public void setNum(Long num) {
        this.num = num;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Integer auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getRefund_id() {
        return refund_id;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public void setRefund_id(String refund_id) {
        this.refund_id = refund_id;
    }

    public Long getTid() {
        return tid;
    }

    public void setTid(Long tid) {
        this.tid = tid;
    }

    public Long getOid() {
        return oid;
    }

    public void setOid(Long oid) {
        this.oid = oid;
    }

    public String getBuyer_nick() {
        return buyer_nick;
    }

    public void setBuyer_nick(String buyer_nick) {
        this.buyer_nick = buyer_nick;
    }

    public String getTotal_fee() {
        return total_fee;
    }

    public void setTotal_fee(String total_fee) {
        this.total_fee = total_fee;
    }

    public String getRefund_fee() {
        return refund_fee;
    }

    public void setRefund_fee(String refund_fee) {
        this.refund_fee = refund_fee;
    }

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public Long getModified() {
        return modified;
    }

    public void setModified(Long modified) {
        this.modified = modified;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGood_status() {
        return good_status;
    }

    public void setGood_status(String good_status) {
        this.good_status = good_status;
    }

    public Integer getHas_good_return() {
        return has_good_return;
    }

    public void setHas_good_return(Integer has_good_return) {
        this.has_good_return = has_good_return;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getLogisticsCompany() {
        return logisticsCompany;
    }

    public void setLogisticsCompany(String logisticsCompany) {
        this.logisticsCompany = logisticsCompany;
    }

    public String getLogisticsCode() {
        return logisticsCode;
    }

    public void setLogisticsCode(String logisticsCode) {
        this.logisticsCode = logisticsCode;
    }
}
