package com.b2c.entity.datacenter.vo;

import java.util.ArrayList;

public class UserShopIdVo {
    private Integer id;
    private String userName;
    private String userPwd;
    private String userSalf;
    private String trueName;
    private String mobile;
    private ArrayList groupId;
    private Integer status;
    private String addTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPwd() {
        return userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }

    public String getUserSalf() {
        return userSalf;
    }

    public void setUserSalf(String userSalf) {
        this.userSalf = userSalf;
    }

    public String getTrueName() {
        return trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public ArrayList getGroupId() {
        return groupId;
    }

    public void setGroupId(ArrayList groupId) {
        this.groupId = groupId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }
}
