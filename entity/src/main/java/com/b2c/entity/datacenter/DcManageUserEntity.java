package com.b2c.entity.datacenter;

import java.util.List;

/***
 * 登入账户列表
 */
public class DcManageUserEntity {
    private  Long id;


    private  String userName;


    private  String userPwd;

    private  String passWord;

    private  String userSalf;

    //真实姓名
    private  String trueName;

    //手机号
    private  String mobile;


    private  Long shopId;

    //状态：0正常,1禁用
    private  Long status;

    //添加时间
    private  String addTime;

    /**
     * 登录时间
     */
    private Integer loginTime;
    //分组名
    private String name;

    private List<DcManageUserShopEntity> shopList;

    public List<DcManageUserShopEntity> getShopList() {
        return shopList;
    }

    public void setShopList(List<DcManageUserShopEntity> shopList) {
        this.shopList = shopList;
    }

    public Integer getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Integer loginTime) {
        this.loginTime = loginTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPwd() {
        return userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }

    public String getUserSalf() {
        return userSalf;
    }

    public void setUserSalf(String userSalf) {
        this.userSalf = userSalf;
    }

    public String getTrueName() {
        return trueName;
    }

    public void setTrueName(String trueName) {
        this.trueName = trueName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }
}
