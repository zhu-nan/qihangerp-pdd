package com.b2c.entity;

import java.math.BigDecimal;

/**
 * @Description:订单商品列表 pbd add 2019/2/21 13:47
 */
public class OrderItemEntity {
    /**
     * id
     */
    private Long id;
    /**
     * 订单id
     */
    private Long orderId;
    /**
     * 商品id
     */
    private int goodsId;
    private String goodsNumber;
    private int specId;
    /**
     * 标题
     */
    private String title;
    /**
     * 图片
     */
    private String image;
    /**
     * 颜色
     */
    private String color;
    /**
     * 尺码
     */
    private String size;
    /**
     * 商品SKU 编码
     */
    private String specNumber;
    /**
     * 商品编码
     */
    /**
     * 商品数量
     */
    private Integer count;
    /**
     * price
     */
    private BigDecimal price;
    /**
     * 商品状态
     */
    private Integer afterSaleState;
    /**
     * 是否能申请售后
     */
    private Integer isAfter;
    /**
     * 折扣价格
     */
    private BigDecimal discountPrice;

    private Long aliItemID; //阿里子订单Id
    /**
     * 商品佣金比例
     */
    private BigDecimal commisionRate;

    private String erpSpec;
    private String newSpec;
    /**
     * 新规格id
     */
    private Integer newSpecId;
    /**
     * 新规格
     */
    private String newSpecNumber;
    private Integer returnedCount;//已退货数量


    /**********商品仓库库存信息**********/
    private Long currentQty;
    private Long lockedQty;
    private Long pickQty;

    public Integer getReturnedCount() {
        return returnedCount;
    }

    public void setReturnedCount(Integer returnedCount) {
        this.returnedCount = returnedCount;
    }

    public String getErpSpec() {
        return erpSpec;
    }

    public void setErpSpec(String erpSpec) {
        this.erpSpec = erpSpec;
    }

    public String getNewSpec() {
        return newSpec;
    }

    public void setNewSpec(String newSpec) {
        this.newSpec = newSpec;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public void setIsAfter(Integer isAfter) {
        this.isAfter = isAfter;
    }

    public Long getCurrentQty() {
        return currentQty;
    }

    public void setCurrentQty(Long currentQty) {
        this.currentQty = currentQty;
    }

    public Long getLockedQty() {
        return lockedQty;
    }

    public void setLockedQty(Long lockedQty) {
        this.lockedQty = lockedQty;
    }

    public Long getPickQty() {
        return pickQty;
    }

    public void setPickQty(Long pickQty) {
        this.pickQty = pickQty;
    }

    public String getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(String goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public String getNewSpecNumber() {
        return newSpecNumber;
    }

    public void setNewSpecNumber(String newSpecNumber) {
        this.newSpecNumber = newSpecNumber;
    }

    public int getSpecId() {
        return specId;
    }

    public void setSpecId(int specId) {
        this.specId = specId;
    }

    public Long getAliItemID() {
        return aliItemID;
    }

    public void setAliItemID(Long aliItemID) {
        this.aliItemID = aliItemID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public int getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(int goodsId) {
        this.goodsId = goodsId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getSpecNumber() {
        return specNumber;
    }

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getAfterSaleState() {
        return afterSaleState;
    }

    public void setAfterSaleState(Integer afterSaleState) {
        this.afterSaleState = afterSaleState;
    }

    public int getIsAfter() {
        return isAfter;
    }

    public void setIsAfter(int isAfter) {
        this.isAfter = isAfter;
    }

    public BigDecimal getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(BigDecimal discountPrice) {
        this.discountPrice = discountPrice;
    }

    public BigDecimal getCommisionRate() {
        return commisionRate;
    }

    public void setCommisionRate(BigDecimal commisionRate) {
        this.commisionRate = commisionRate;
    }

    public Integer getNewSpecId() {
        return newSpecId;
    }

    public void setNewSpecId(Integer newSpecId) {
        this.newSpecId = newSpecId;
    }
}
