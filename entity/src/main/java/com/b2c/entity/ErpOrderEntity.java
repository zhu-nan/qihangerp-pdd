package com.b2c.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * 描述：
 * erp订单实体
 *
 * @author qlp
 * @date 2019-09-17 11:43
 */
public class ErpOrderEntity {

    private Long id;//` bigint(20) NOT NULL AUTO_INCREMENT,
    private String order_num;//` varchar(35) NOT NULL COMMENT '订单编码',
    private BigDecimal totalAmount;//` decimal(10,2) NOT NULL COMMENT '应付款总金额，totalAmount = ∑itemAmount + shippingFee，单位为元',
    private BigDecimal shippingFee;//` decimal(11,2) NOT NULL COMMENT '运费',
    private Long orderTime;//` datetime NOT NULL COMMENT '下单时间',
    private Long createTime;//` datetime NOT NULL COMMENT '创建时间',
    private Long modifyTime;//` datetime NOT NULL COMMENT '修改时间',
    private Long confirmedTime;//` datetime DEFAULT NULL COMMENT '确认时间',
    private Long pickingTime;//` datetime DEFAULT NULL COMMENT '拣货时间',
    private Long pickedTime;//` datetime DEFAULT NULL COMMENT '确认时间',
    private Long stockOutTime;//` datetime DEFAULT NULL COMMENT '确认时间',
    private Long deliveryTime;//` datetime DEFAULT NULL COMMENT '发货时间',
    private String logisticsCompany;//` varchar(30) DEFAULT NULL COMMENT '物流公司',
    private String logisticsCompanyCode;//` varchar(30) DEFAULT NULL COMMENT '物流公司编码',
    private String logisticsCode;//` varchar(30) DEFAULT NULL COMMENT '物流单号',
    private String remark;//` varchar(50) DEFAULT '' COMMENT '备注，1688指下单时的备注',
    private String buyerFeedback;//` varchar(500) DEFAULT NULL COMMENT '买家留言，不超过500字',
    private String sellerMemo;//` varchar(100) DEFAULT NULL COMMENT '卖家备忘信息',
    private String contactPerson;//` varchar(20) NOT NULL DEFAULT '' COMMENT '收货人',
    private String mobile;//` varchar(20) NOT NULL DEFAULT '' COMMENT '收货人手机号',
    private String province;//` varchar(20) DEFAULT NULL COMMENT '省',
    private String city;//` varchar(30) DEFAULT NULL COMMENT '市',
    private String area;//` varchar(30) DEFAULT NULL COMMENT '区县',
    private String areaCode;//` varchar(10) DEFAULT NULL,
    private String town;//` varchar(30) DEFAULT NULL COMMENT '街道',
    private String townCode;//` varchar(20) DEFAULT NULL,
    private String address;//` varchar(50) NOT NULL DEFAULT '' COMMENT '收货地址',
    private String source;//` varchar(10) NOT NULL DEFAULT '' COMMENT '订单来源（ALIBABA，TAOBAO，YOUZAN，YUNGOU，ERP）',

    private Integer status;//状态，状态（0未处理1拣货中2已拣货3已发货）

//    private Integer isPrint;//是否可打印快递单(0可打印，1不可打印)

    private Integer saleType;//销售类型（0样品1实售）
    private Integer shopId;//店铺ID
    private String shopName;//店铺名
    private Integer logisticsPrintStatus;//物流面单打印状态
    private Integer logisticsPrintCount;//物流面单打印次数
    private Long logisticsPrintTime;//物流面单最后打印时间
    private Integer logisticsPrintType;//快递单打印类型1手写2打印


    public Integer getLogisticsPrintType() {
        return logisticsPrintType;
    }

    public void setLogisticsPrintType(Integer logisticsPrintType) {
        this.logisticsPrintType = logisticsPrintType;
    }

    public Integer getLogisticsPrintStatus() {
        return logisticsPrintStatus;
    }

    public void setLogisticsPrintStatus(Integer logisticsPrintStatus) {
        this.logisticsPrintStatus = logisticsPrintStatus;
    }

    public Integer getLogisticsPrintCount() {
        return logisticsPrintCount;
    }

    public void setLogisticsPrintCount(Integer logisticsPrintCount) {
        this.logisticsPrintCount = logisticsPrintCount;
    }

    public Long getLogisticsPrintTime() {
        return logisticsPrintTime;
    }

    public void setLogisticsPrintTime(Long logisticsPrintTime) {
        this.logisticsPrintTime = logisticsPrintTime;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public Integer getSaleType() {
        return saleType;
    }

    public void setSaleType(Integer saleType) {
        this.saleType = saleType;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrder_num() {
        return order_num;
    }

    public void setOrder_num(String order_num) {
        this.order_num = order_num;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getShippingFee() {
        return shippingFee;
    }

    public void setShippingFee(BigDecimal shippingFee) {
        this.shippingFee = shippingFee;
    }

    public Long getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Long orderTime) {
        this.orderTime = orderTime;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Long modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Long getConfirmedTime() {
        return confirmedTime;
    }

    public void setConfirmedTime(Long confirmedTime) {
        this.confirmedTime = confirmedTime;
    }

    public Long getPickingTime() {
        return pickingTime;
    }

    public void setPickingTime(Long pickingTime) {
        this.pickingTime = pickingTime;
    }

    public Long getPickedTime() {
        return pickedTime;
    }

    public void setPickedTime(Long pickedTime) {
        this.pickedTime = pickedTime;
    }

    public Long getStockOutTime() {
        return stockOutTime;
    }

    public void setStockOutTime(Long stockOutTime) {
        this.stockOutTime = stockOutTime;
    }

    public Long getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Long deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getLogisticsCompany() {
        return logisticsCompany;
    }

    public void setLogisticsCompany(String logisticsCompany) {
        this.logisticsCompany = logisticsCompany;
    }

    public String getLogisticsCompanyCode() {
        return logisticsCompanyCode;
    }

    public void setLogisticsCompanyCode(String logisticsCompanyCode) {
        this.logisticsCompanyCode = logisticsCompanyCode;
    }

    public String getLogisticsCode() {
        return logisticsCode;
    }

    public void setLogisticsCode(String logisticsCode) {
        this.logisticsCode = logisticsCode;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getBuyerFeedback() {
        return buyerFeedback;
    }

    public void setBuyerFeedback(String buyerFeedback) {
        this.buyerFeedback = buyerFeedback;
    }

    public String getSellerMemo() {
        return sellerMemo;
    }

    public void setSellerMemo(String sellerMemo) {
        this.sellerMemo = sellerMemo;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getTownCode() {
        return townCode;
    }

    public void setTownCode(String townCode) {
        this.townCode = townCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

//    public Integer getIsPrint() {
//        return isPrint;
//    }
//
//    public void setIsPrint(Integer isPrint) {
//        this.isPrint = isPrint;
//    }
}
