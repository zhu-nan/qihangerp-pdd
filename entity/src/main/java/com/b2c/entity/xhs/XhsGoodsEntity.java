package com.b2c.entity.xhs;

public class XhsGoodsEntity {

    private Long id;//` BIGINT(20) NOT NULL AUTO_INCREMENT,
    private Integer shopId;
    private String name;//` VARCHAR(50) NULL DEFAULT NULL COMMENT '商品名' COLLATE 'utf8_general_ci',
    private String goodsNum;//` VARCHAR(50) NULL DEFAULT NULL COMMENT '商品编码' COLLATE 'utf8_general_ci',
    private String imageUrl;//` VARCHAR(255) NULL DEFAULT NULL COMMENT '主图Url' COLLATE 'utf8_general_ci',
    private String shortName;//` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
    private String spuId;//` VARCHAR(50) NULL DEFAULT NULL COMMENT '小红书商品id' COLLATE 'utf8_general_ci',
    private String videoUrl;//` VARCHAR(255) NULL DEFAULT NULL COMMENT '视频URL' COLLATE 'utf8_general_ci',
    private String desc;//` VARCHAR(255) NULL DEFAULT NULL COMMENT '商品描述' COLLATE 'utf8_general_ci',
    private String descImages;//` VARCHAR(2000) NULL DEFAULT NULL COMMENT '商品描述图片' COLLATE 'utf8_general_ci',
    private Double price;//` DOUBLE NULL DEFAULT NULL COMMENT '商品价格',
    private Integer stock;//` INT(11) NULL DEFAULT NULL COMMENT '库存',
    private String erpCode;//` VARCHAR(50) NULL DEFAULT NULL COMMENT 'SKU商家编码' COLLATE 'utf8_general_ci',
    private String skuId;//` VARCHAR(50) NULL DEFAULT NULL COMMENT '小红书规格id' COLLATE 'utf8_general_ci',
    private String spec;//` VARCHAR(50) NULL DEFAULT NULL COMMENT '规格' COLLATE 'utf8_general_ci',
    private String createOn;//` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    private String modifyOn;//` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    private Integer erpSkuId;

    public Integer getErpSkuId() {
        return erpSkuId;
    }

    public void setErpSkuId(Integer erpSkuId) {
        this.erpSkuId = erpSkuId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(String goodsNum) {
        this.goodsNum = goodsNum;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getSpuId() {
        return spuId;
    }

    public void setSpuId(String spuId) {
        this.spuId = spuId;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDescImages() {
        return descImages;
    }

    public void setDescImages(String descImages) {
        this.descImages = descImages;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public String getErpCode() {
        return erpCode;
    }

    public void setErpCode(String erpCode) {
        this.erpCode = erpCode;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getSpec() {
        return spec;
    }

    public void setSpec(String spec) {
        this.spec = spec;
    }

    public String getCreateOn() {
        return createOn;
    }

    public void setCreateOn(String createOn) {
        this.createOn = createOn;
    }

    public String getModifyOn() {
        return modifyOn;
    }

    public void setModifyOn(String modifyOn) {
        this.modifyOn = modifyOn;
    }
}
