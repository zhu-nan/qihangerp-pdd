package com.b2c.entity.erp.vo;

import java.util.List;

/**
 * @author ly
 * @desc 分类返回
 * @date 2019/3/21
 */
public class CategoryRe {
    private String name;
    private Integer id;
    private List<CategoryRe> child;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<CategoryRe> getChild() {
        return child;
    }

    public void setChild(List<CategoryRe> child) {
        this.child = child;
    }
}
