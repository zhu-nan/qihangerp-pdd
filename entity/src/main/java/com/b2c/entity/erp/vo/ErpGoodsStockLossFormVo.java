package com.b2c.entity.erp.vo;

import java.util.List;

public class ErpGoodsStockLossFormVo {
    private  Long id;

    //表单号
    private  String number;

    //创建时间
    private  Long createTime;

    //创建用户名
    private  String createUserName;

    private List<ErpGoodsStockLossItemVo> items;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public List<ErpGoodsStockLossItemVo> getItems() {
        return items;
    }

    public void setItems(List<ErpGoodsStockLossItemVo> items) {
        this.items = items;
    }
}
