package com.b2c.entity.erp.vo;

public class ShopGoodsSkuLinkErpSkuVo {
    private Integer erpSkuId;
    private Long shopGoodsSkuId;
    public Integer getErpSkuId() {
        return erpSkuId;
    }
    public void setErpSkuId(Integer erpSkuId) {
        this.erpSkuId = erpSkuId;
    }
    public Long getShopGoodsSkuId() {
        return shopGoodsSkuId;
    }
    public void setShopGoodsSkuId(Long shopGoodsSkuId) {
        this.shopGoodsSkuId = shopGoodsSkuId;
    }
    
}
