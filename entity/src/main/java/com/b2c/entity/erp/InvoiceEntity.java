package com.b2c.entity.erp;

/**
 * 描述：
 * 购货单、销货单Entity
 *
 * @author qlp
 * @date 2019-03-21 14:38
 */
public class InvoiceEntity {
    private Long id;
    private Long contactId;//供应商id 供应商id\客户id\订单收货人id
    private String contactName;//供应商
    private String billNo;//单据编号
    private String contractNo;  //合同号
    private Integer userId;//制单人id
    private String userName;//制单人
    private String transType;//交易类型,见枚举InvoiceTransTypeEnum
    private String transTypeName;//交易类型名称
    private Double totalAmount;//购货总金额
    private Double amount;//折扣后金额
    private Double rpAmount;//本次付款
    private String billDate;//单据日期
    private String billTime;//时间
    private String description;//备注
    private Double arrears;//本次欠款
    private Double disRate;//折扣率
    private Double disAmount;//折扣金额
    private Double totalDiscount;//总折扣
    private Long totalQuantity;//总数量
    private Long inQuantity;//已入库数量（已出库数量）
    private Long qualifiedQuantity;//合格数量
    private Long abnormalQuantity; //异常数量
    //    private Double totalArrears;//总欠款
    private Integer billStatus;//订单状态 订单状态 0待审核1已审核2已作废
    private String checkName;//采购单审核人
    private Integer checked;//采购单审核状态0待审核1已审核
    private String billType;//PO采购订单 OI其他入库 PUR采购入库 BAL初期余额
    private Long createTime;//创建时间
    private Long modifyTime;//更新时间
    private Integer salesId;//销售人员ID
    private Double customerFree;//客户承担费用
    private Integer hxStateCode;//核销状态 0未付款  1部分付款  2全部付款
    private Double hxAmount;//本次核销金额
    private Double payment;//本次预收款(销售)
    private Double discount;//整单折扣(销售)
    private Double freight;//运费
    private String srcOrderNo;//订单编号(销售)
    private String srcOrderId;//订单id
    private String locationId;//仓库id多个,分割
    private String inLocationId;//调入仓库ID多个,分割
    private String outLocationId;//调出仓库ID多个,分割
    private Integer isDelete;//1删除  0正常
    private String serialno;//系列号
    private String logisticsCompany;
    private String logisticsCompanyCode;
    private String logisticsNumber;
    private Integer specId;//规格ID
    private String specNumber;
    private String goodsName;
    private Long checkoutTime;//检验时间
    private String checkoutTimes;
    private String checkoutName;//检验人
    private Long stockInTime;//入库时间
    private String stockInTimes;
    private String stockInName;//入库人

    private Integer checkoutStatus;

    private String printTime;//打印时间


    public String getContractNo() {
        return contractNo;
    }

    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    public String getPrintTime() {
        return printTime;
    }

    public void setPrintTime(String printTime) {
        this.printTime = printTime;
    }

    public Long getStockInTime() {
        return stockInTime;
    }

    public void setStockInTime(Long stockInTime) {
        this.stockInTime = stockInTime;
    }

    public String getStockInTimes() {
        return stockInTimes;
    }

    public void setStockInTimes(String stockInTimes) {
        this.stockInTimes = stockInTimes;
    }

    public String getStockInName() {
        return stockInName;
    }

    public void setStockInName(String stockInName) {
        this.stockInName = stockInName;
    }

    public String getCheckoutName() {
        return checkoutName;
    }

    public void setCheckoutName(String checkoutName) {
        this.checkoutName = checkoutName;
    }

    public String getCheckoutTimes() {
        return checkoutTimes;
    }

    public void setCheckoutTimes(String checkoutTimes) {
        this.checkoutTimes = checkoutTimes;
    }

    public Long getCheckoutTime() {
        return checkoutTime;
    }

    public void setCheckoutTime(Long checkoutTime) {
        this.checkoutTime = checkoutTime;
    }

    public Long getQualifiedQuantity() {
        return qualifiedQuantity;
    }

    public void setQualifiedQuantity(Long qualifiedQuantity) {
        this.qualifiedQuantity = qualifiedQuantity;
    }

    public String getBillTime() {
        return billTime;
    }

    public void setBillTime(String billTime) {
        this.billTime = billTime;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public Integer getSpecId() {
        return specId;
    }

    public void setSpecId(Integer specId) {
        this.specId = specId;
    }

    public String getSpecNumber() {
        return specNumber;
    }

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    public String getLogisticsCompany() {
        return logisticsCompany;
    }

    public void setLogisticsCompany(String logisticsCompany) {
        this.logisticsCompany = logisticsCompany;
    }

    public String getLogisticsCompanyCode() {
        return logisticsCompanyCode;
    }

    public void setLogisticsCompanyCode(String logisticsCompanyCode) {
        this.logisticsCompanyCode = logisticsCompanyCode;
    }

    public String getLogisticsNumber() {
        return logisticsNumber;
    }

    public void setLogisticsNumber(String logisticsNumber) {
        this.logisticsNumber = logisticsNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Long getInQuantity() {
        return inQuantity;
    }

    public void setInQuantity(Long inQuantity) {
        this.inQuantity = inQuantity;
    }

    public Long getAbnormalQuantity() {
        return abnormalQuantity;
    }

    public void setAbnormalQuantity(Long abnormalQuantity) {
        this.abnormalQuantity = abnormalQuantity;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public String getTransTypeName() {
        return transTypeName;
    }

    public void setTransTypeName(String transTypeName) {
        this.transTypeName = transTypeName;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getRpAmount() {
        return rpAmount;
    }

    public void setRpAmount(Double rpAmount) {
        this.rpAmount = rpAmount;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getArrears() {
        return arrears;
    }

    public void setArrears(Double arrears) {
        this.arrears = arrears;
    }

    public Double getDisRate() {
        return disRate;
    }

    public void setDisRate(Double disRate) {
        this.disRate = disRate;
    }

    public Double getDisAmount() {
        return disAmount;
    }

    public void setDisAmount(Double disAmount) {
        this.disAmount = disAmount;
    }

    public Double getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(Double totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public Long getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Long totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

//    public Double getTotalArrears() {
//        return totalArrears;
//    }
//
//    public void setTotalArrears(Double totalArrears) {
//        this.totalArrears = totalArrears;
//    }

    public Integer getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(Integer billStatus) {
        this.billStatus = billStatus;
    }

    public String getCheckName() {
        return checkName;
    }

    public void setCheckName(String checkName) {
        this.checkName = checkName;
    }

    public Integer getChecked() {
        return checked;
    }

    public void setChecked(Integer checked) {
        this.checked = checked;
    }

    public String getBillType() {
        return billType;
    }

    public void setBillType(String billType) {
        this.billType = billType;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Long modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Integer getSalesId() {
        return salesId;
    }

    public void setSalesId(Integer salesId) {
        this.salesId = salesId;
    }

    public Double getCustomerFree() {
        return customerFree;
    }

    public void setCustomerFree(Double customerFree) {
        this.customerFree = customerFree;
    }

    public Integer getHxStateCode() {
        return hxStateCode;
    }

    public void setHxStateCode(Integer hxStateCode) {
        this.hxStateCode = hxStateCode;
    }

    public Double getHxAmount() {
        return hxAmount;
    }

    public void setHxAmount(Double hxAmount) {
        this.hxAmount = hxAmount;
    }

    public Double getPayment() {
        return payment;
    }

    public void setPayment(Double payment) {
        this.payment = payment;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public String getSrcOrderNo() {
        return srcOrderNo;
    }

    public void setSrcOrderNo(String srcOrderNo) {
        this.srcOrderNo = srcOrderNo;
    }

    public String getSrcOrderId() {
        return srcOrderId;
    }

    public void setSrcOrderId(String srcOrderId) {
        this.srcOrderId = srcOrderId;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getInLocationId() {
        return inLocationId;
    }

    public void setInLocationId(String inLocationId) {
        this.inLocationId = inLocationId;
    }

    public String getOutLocationId() {
        return outLocationId;
    }

    public void setOutLocationId(String outLocationId) {
        this.outLocationId = outLocationId;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public String getSerialno() {
        return serialno;
    }

    public void setSerialno(String serialno) {
        this.serialno = serialno;
    }

    public Integer getCheckoutStatus() {
        return checkoutStatus;
    }

    public void setCheckoutStatus(Integer checkoutStatus) {
        this.checkoutStatus = checkoutStatus;
    }

    public Double getFreight() {
        return freight;
    }

    public void setFreight(Double freight) {
        this.freight = freight;
    }
}
