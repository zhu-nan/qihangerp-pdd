package com.b2c.entity.erp.enums;

/**
 * 描述：客户累心
 *
 * @author qlp
 * @date 2019-03-06 09:20
 */
public enum ContactTypeEnum {
    Supplier("供应商", 10),
    Customer("客户", 20),
    OrderReceiver("订单收货人", 30);

    private String name;
    private int index;

    // 构造方法
    private ContactTypeEnum(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (ContactTypeEnum c : ContactTypeEnum.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
