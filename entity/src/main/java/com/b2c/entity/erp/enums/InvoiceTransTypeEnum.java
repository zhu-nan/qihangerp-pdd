package com.b2c.entity.erp.enums;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-03-06 09:20
 */
public enum InvoiceTransTypeEnum {
    // Buy("购货", "BUY"),
    Purchase("入库采购", "Purchase"),
    // Manufacture("生产单", "Manufacture"),
    // BuyReturn("退货", "BUYR"),
    // Sale("销售", "SALE"),
    // Order("线上订单", "ORDER"),
    OrderDaiFa("订单代发采购", "OrderDaiFa"),
    DaiFaRefund("代发退货", "DaiFaRefund"),
    // SaleReturn("销售退货", "SALER"),
    // Other("其他入库", "OTHER"),
    PUR_RETURN("采购退货","PUR_RETURN");

    private String name;
    private String index;

    // 构造方法
    private InvoiceTransTypeEnum(String name, String index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(String index) {
        for (InvoiceTransTypeEnum c : InvoiceTransTypeEnum.values()) {
            if (c.getIndex().equals(index)) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }
}
