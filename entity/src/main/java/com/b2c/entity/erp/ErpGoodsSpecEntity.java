
package com.b2c.entity.erp;

/**
 * 描述：
 * Entity
 * 商品规格库存管理
 *
 * @author pbd
 * @date 2019-06-25 11:51
 */
public class ErpGoodsSpecEntity {
    private int id;

    //商品id
    private int goodsId;

    //起初数量
    private long quantity;

    //规格名
    private String specName = "";

    //规格编码
    private String specNumber = "";

    private String goodsNumber;//商品编码

    //库存条形码
    private String barCode;

    //预计采购价
    private double purPrice;

    //预计销售价
    private double salePrice;

    //单位成本
    private double unitCost;

    //期初总价
    private double amount;

    //会员价
    private double vipPrice;

    //批发价
    private double wholesalePrice;

    //折扣1
    private double discountRate1;

    //折扣2
    private double discountRate2;

    //""
    private String remark;

    //初期设置
    private String propertys;

    //状态
    private int status;

    //最低库存（预警）
    private long lowQty;

    //最高库存（预警）
    private long highQty;

    //仓库id
    private Integer locationId;

    //仓库名
    private String locationName = "";

    //辅助属性分类
    private String skuAssistId;

    //图片路径
    private String files;

    //0启用   1禁用
    private int disable;

    //属性
    private String property = "";

    //保质期
    private double safeDays;


    private double advanceDay;

    //是否保修 0否1是
    private int isWarranty;

    //0正常  1删除
    private int isDelete;

    //库存预警
    private String warehouseWarning;

    //当前库存
    private long currentQty;

    //有赞商品ID
    private long yzItemId;
    //商品名
    private String goodTitle;

    private Integer colorId;
    private Integer sizeId;
    private Integer styleId;


    private String colorValue;
    private String sizeValue;
    private String styleValue;
    private String colorImage;
    private Integer erpContactId;

    public String getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(String goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public Integer getStyleId() {
        return styleId;
    }

    public void setStyleId(Integer styleId) {
        this.styleId = styleId;
    }

    public String getStyleValue() {
        return styleValue;
    }

    public void setStyleValue(String styleValue) {
        this.styleValue = styleValue;
    }



    public String getColorImage() {
        return colorImage;
    }

    public void setColorImage(String colorImage) {
        this.colorImage = colorImage;
    }

    public String getColorValue() {
        return colorValue;
    }

    public void setColorValue(String colorValue) {
        this.colorValue = colorValue;
    }

    public String getSizeValue() {
        return sizeValue;
    }

    public void setSizeValue(String sizeValue) {
        this.sizeValue = sizeValue;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(int goodsId) {
        this.goodsId = goodsId;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public String getSpecName() {
        return this.specName;
    }

    ;

    public void setSpecName(String specName) {
        this.specName = specName;
    }

    public String getSpecNumber() {
        return this.specNumber;
    }

    ;

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    public String getBarCode() {
        return this.barCode;
    }

    ;

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public double getPurPrice() {
        return this.purPrice;
    }

    ;

    public void setPurPrice(double purPrice) {
        this.purPrice = purPrice;
    }

    public double getSalePrice() {
        return this.salePrice;
    }

    ;

    public void setSalePrice(double salePrice) {
        this.salePrice = salePrice;
    }

    public double getUnitCost() {
        return this.unitCost;
    }

    ;

    public void setUnitCost(double unitCost) {
        this.unitCost = unitCost;
    }

    public double getAmount() {
        return this.amount;
    }

    ;

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getVipPrice() {
        return this.vipPrice;
    }

    ;

    public void setVipPrice(double vipPrice) {
        this.vipPrice = vipPrice;
    }

    public double getWholesalePrice() {
        return this.wholesalePrice;
    }

    ;

    public void setWholesalePrice(double wholesalePrice) {
        this.wholesalePrice = wholesalePrice;
    }

    public double getDiscountRate1() {
        return this.discountRate1;
    }

    ;

    public void setDiscountRate1(double discountRate1) {
        this.discountRate1 = discountRate1;
    }

    public double getDiscountRate2() {
        return this.discountRate2;
    }

    ;

    public void setDiscountRate2(double discountRate2) {
        this.discountRate2 = discountRate2;
    }

    public String getRemark() {
        return this.remark;
    }

    ;

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getPropertys() {
        return this.propertys;
    }

    ;

    public void setPropertys(String propertys) {
        this.propertys = propertys;
    }

    public int getStatus() {
        return this.status;
    }

    ;

    public void setStatus(int status) {
        this.status = status;
    }

    public long getLowQty() {
        return this.lowQty;
    }

    ;

    public void setLowQty(long lowQty) {
        this.lowQty = lowQty;
    }

    public long getHighQty() {
        return this.highQty;
    }

    ;

    public void setHighQty(long highQty) {
        this.highQty = highQty;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public String getLocationName() {
        return this.locationName;
    }

    ;

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getSkuAssistId() {
        return this.skuAssistId;
    }

    ;

    public void setSkuAssistId(String skuAssistId) {
        this.skuAssistId = skuAssistId;
    }

    public String getFiles() {
        return this.files;
    }

    ;

    public void setFiles(String files) {
        this.files = files;
    }

    public int getDisable() {
        return this.disable;
    }

    ;

    public void setDisable(int disable) {
        this.disable = disable;
    }

    public String getProperty() {
        return this.property;
    }

    ;

    public void setProperty(String property) {
        this.property = property;
    }

    public double getSafeDays() {
        return this.safeDays;
    }

    ;

    public void setSafeDays(double safeDays) {
        this.safeDays = safeDays;
    }

    public double getAdvanceDay() {
        return this.advanceDay;
    }

    ;

    public void setAdvanceDay(double advanceDay) {
        this.advanceDay = advanceDay;
    }

    public int getIsWarranty() {
        return this.isWarranty;
    }

    ;

    public void setIsWarranty(int isWarranty) {
        this.isWarranty = isWarranty;
    }

    public int getIsDelete() {
        return this.isDelete;
    }

    ;

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }

    public String getWarehouseWarning() {
        return this.warehouseWarning;
    }

    ;

    public void setWarehouseWarning(String warehouseWarning) {
        this.warehouseWarning = warehouseWarning;
    }

    public long getCurrentQty() {
        return this.currentQty;
    }

    ;

    public void setCurrentQty(long currentQty) {
        this.currentQty = currentQty;
    }

    public long getYzItemId() {
        return this.yzItemId;
    }

    ;

    public void setYzItemId(long yzItemId) {
        this.yzItemId = yzItemId;
    }


    public Integer getColorId() {
        return colorId;
    }

    public void setColorId(Integer colorId) {
        this.colorId = colorId;
    }

    public Integer getSizeId() {
        return sizeId;
    }

    public void setSizeId(Integer sizeId) {
        this.sizeId = sizeId;
    }

    public String getGoodTitle() {
        return goodTitle;
    }

    public void setGoodTitle(String goodTitle) {
        this.goodTitle = goodTitle;
    }

    public Integer getErpContactId() {
        return erpContactId;
    }

    public void setErpContactId(Integer erpContactId) {
        this.erpContactId = erpContactId;
    }
}