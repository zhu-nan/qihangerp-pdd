package com.b2c.entity.erp.vo;

/**
 * @Description:库存商品信息 pbd add 2019/7/29 15:35
 */
public class ErpGoodsSpecVo {
    private Integer id;
    /**
     * 库存商品Id
     */
    private Integer goodsId;
    /**
     * 商品名
     */
    private String name;
    /**
     * 商品编码
     */
    private String goodNumber;
    /**
     * 单位名
     */
    private String unitName;
    /**
     * 规格id
     */
    private Integer specId;
    /**
     * 规格名
     */
    private String specName;
    /**
     * 商品规格编码
     */
    private String specNumber;
    /**
     * 商品所属仓库Id
     */
    private Integer locationId;
    /**
     * 商品所属库区Id
     */
    private Integer reservoirId;
    /**
     * 商品所属货架Id
     */
    private Integer shelfId;
    /**
     * 商品当前库存数量
     */
    private Integer currentQty;

    public Integer getSpecId() {
        return specId;
    }

    public void setSpecId(Integer specId) {
        this.specId = specId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSpecName() {
        return specName;
    }

    public void setSpecName(String specName) {
        this.specName = specName;
    }

    public String getSpecNumber() {
        return specNumber;
    }

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public Integer getReservoirId() {
        return reservoirId;
    }

    public void setReservoirId(Integer reservoirId) {
        this.reservoirId = reservoirId;
    }

    public Integer getShelfId() {
        return shelfId;
    }

    public void setShelfId(Integer shelfId) {
        this.shelfId = shelfId;
    }

    public Integer getCurrentQty() {
        return currentQty;
    }

    public void setCurrentQty(Integer currentQty) {
        this.currentQty = currentQty;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodNumber() {
        return goodNumber;
    }

    public void setGoodNumber(String goodNumber) {
        this.goodNumber = goodNumber;
    }
}
