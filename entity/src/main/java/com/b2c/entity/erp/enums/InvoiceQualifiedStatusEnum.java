package com.b2c.entity.erp.enums;

public enum InvoiceQualifiedStatusEnum {
    NoQualified("不合格", 0),
    Qualified("合格", 1);

    private String name;
    private int index;

    InvoiceQualifiedStatusEnum(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (InvoiceQualifiedStatusEnum c : InvoiceQualifiedStatusEnum.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
