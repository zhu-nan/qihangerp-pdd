package com.b2c.entity.erp.vo;

import java.util.List;

/**
 * 描述：
 * 库存商品管理实体
 *
 * @author ly
 * @date 2019-3-21 17:36
 */
public class ErpGoodsSpecStockVo {
    private Integer id;
    private Integer goodsId;
    private String goodsName;
    private String number;  //商品编码
    private Integer categoryId;
    private String categoryName;
    private Integer specId;
    private String specNumber;  //规格编码
    private Double purPrice;
    private String image;
//    private String specName;  //规格名
    private String colorValue;//颜色
    private String colorImage;//颜色
    private String sizeValue;//尺码
    private String styleValue;//款式
    private Long qty;  //当前数量
    private Long inQty;//入库数量
    private Long outQty;
    private Long lockedQty;  //锁定库存
    private Long lockedQty1;
    private Integer disable; //0启用   1禁用
    private Integer locationId;
    private String locationName; //仓库名
    private String locationNumber; //仓库名
//    private Integer reservoirId;
//    private String reservoirName;
//    private Integer shelfId;
//    private String shelfName;
    private String unitName; //单位名称
//    private String houseName;
    private String attr1;
    private String attr2;
    private Long currentQty;
    private Double costPrice;

    public String getAttr1() {
        return attr1;
    }

    public void setAttr1(String attr1) {
        this.attr1 = attr1;
    }

    public String getAttr2() {
        return attr2;
    }

    public void setAttr2(String attr2) {
        this.attr2 = attr2;
    }
    private List<ErpGoodsSpecStockListVo> stockList;

    public Double getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(Double costPrice) {
        this.costPrice = costPrice;
    }

    public ErpGoodsSpecStockVo() {
    }

//    public ErpGoodsSpecStockVo(Integer id, Integer goodsId, String goodsName, String number, Integer categoryId, String categoryName, Integer specId, String specNumber, String colorValue, String sizeValue, String styleValue, Long currentQty, Integer disable, Integer locationId, String locationName, String unitName) {
//        this.id = id;
//        this.goodsId = goodsId;
//        this.goodsName = goodsName;
//        this.number = number;
//        this.categoryId = categoryId;
//        this.categoryName = categoryName;
//        this.specId = specId;
//        this.specNumber = specNumber;
//        this.colorValue = colorValue;
//        this.sizeValue = sizeValue;
//        this.styleValue = styleValue;
//        this.currentQty = currentQty;
//        this.disable = disable;
//        this.locationId = locationId;
//        this.locationName = locationName;
//        this.unitName = unitName;
//    }


    public String getColorImage() {
        return colorImage;
    }

    public void setColorImage(String colorImage) {
        this.colorImage = colorImage;
    }

    public Long getLockedQty() {
        return lockedQty;
    }

    public void setLockedQty(Long lockedQty) {
        this.lockedQty = lockedQty;
    }

    public List<ErpGoodsSpecStockListVo> getStockList() {
        return stockList;
    }

    public void setStockList(List<ErpGoodsSpecStockListVo> stockList) {
        this.stockList = stockList;
    }
    public String getLocationNumber() {
        return locationNumber;
    }

    public void setLocationNumber(String locationNumber) {
        this.locationNumber = locationNumber;
    }

    public Integer getSpecId() {
        return specId;
    }

    public void setSpecId(Integer specId) {
        this.specId = specId;
    }

    public String getColorValue() {
        return colorValue;
    }

    public void setColorValue(String colorValue) {
        this.colorValue = colorValue;
    }

    public String getSizeValue() {
        return sizeValue;
    }

    public void setSizeValue(String sizeValue) {
        this.sizeValue = sizeValue;
    }

    public String getStyleValue() {
        return styleValue;
    }

    public void setStyleValue(String styleValue) {
        this.styleValue = styleValue;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getSpecNumber() {
        return specNumber;
    }

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    //    public String getSpecName() {
//        return specName;
//    }
//
//    public void setSpecName(String specName) {
//        this.specName = specName;
//    }



    public Long getQty() {
        return qty;
    }

    public void setQty(Long qty) {
        this.qty = qty;
    }

    public Integer getDisable() {
        return disable;
    }

    public void setDisable(Integer disable) {
        this.disable = disable;
    }


//    public String getHouseName() {
//        return houseName;
//    }
//
//    public void setHouseName(String houseName) {
//        this.houseName = houseName;
//    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

//    public Integer getReservoirId() {
//        return reservoirId;
//    }
//
//    public void setReservoirId(Integer reservoirId) {
//        this.reservoirId = reservoirId;
//    }
//
//    public String getReservoirName() {
//        return reservoirName;
//    }
//
//    public void setReservoirName(String reservoirName) {
//        this.reservoirName = reservoirName;
//    }
//
//    public Integer getShelfId() {
//        return shelfId;
//    }
//
//    public void setShelfId(Integer shelfId) {
//        this.shelfId = shelfId;
//    }
//
//    public String getShelfName() {
//        return shelfName;
//    }
//
//    public void setShelfName(String shelfName) {
//        this.shelfName = shelfName;
//    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public Long getCurrentQty() {
        return currentQty;
    }

    public void setCurrentQty(Long currentQty) {
        this.currentQty = currentQty;
    }

    public Long getInQty() {
        return inQty;
    }

    public void setInQty(Long inQty) {
        this.inQty = inQty;
    }

    public Long getOutQty() {
        return outQty;
    }

    public void setOutQty(Long outQty) {
        this.outQty = outQty;
    }

    public Long getLockedQty1() {
        return lockedQty1;
    }

    public void setLockedQty1(Long lockedQty1) {
        this.lockedQty1 = lockedQty1;
    }

    public Double getPurPrice() {
        return purPrice;
    }

    public void setPurPrice(Double purPrice) {
        this.purPrice = purPrice;
    }
}
