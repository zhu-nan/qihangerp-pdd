package com.b2c.entity.erp.enums;

public enum EnumStockOutType {
    //出库类型1订单拣货出库2采购退货出库3盘点出库4报损出库
    OrderPick("订单拣货出库", 1),
    PurchaseReturn("采购退货出库", 2),
    PanDian("盘点出库", 3),
    Loss("报损出库", 4),
    Disable("禁用", 0);

    private String name;
    private int index;

    // 构造方法
    private EnumStockOutType(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (EnumStockOutType c : EnumStockOutType.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }


    public int getIndex() {
        return index;
    }




}
