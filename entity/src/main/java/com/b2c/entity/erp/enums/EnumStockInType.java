package com.b2c.entity.erp.enums;

public enum EnumStockInType {
    //入库类型1采购入库2退货入库3盘点入库
    Purchase("采购入库", 1),
    OrderReturn("销售退货入库", 2),
    PanDian("盘点入库", 3),
    Disable("禁用", 0);

    private String name;
    private int index;

    // 构造方法
    private EnumStockInType(String name, int index) {
        this.name = name;
        this.index = index;
    }

    // 普通方法
    public static String getName(int index) {
        for (EnumStockInType c : EnumStockInType.values()) {
            if (c.getIndex() == index) {
                return c.name;
            }
        }
        return null;
    }

    // get set 方法
    public String getName() {
        return name;
    }


    public int getIndex() {
        return index;
    }




}
