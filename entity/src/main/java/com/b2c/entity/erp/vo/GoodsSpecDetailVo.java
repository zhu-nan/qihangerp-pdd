package com.b2c.entity.erp.vo;

/**
 * 描述：
 * 商品规格详情vo
 *
 * @author qlp
 * @date 2019-05-30 14:10
 */
public class GoodsSpecDetailVo {

    private Integer goodsId;//商品id
    private String goodsName;//商品名称
    private String goodsNumber;//商品编号
    private Integer specId;//商品规格id
//    private String specName;//规格名称
    private String specNumber;//规格编号
    private String colorValue;//颜色
    private String sizeValue;//尺码
    private String styleValue;//款式
    private Long currentQty;    //库存
//    private Integer locationId;//仓库id
//    private String locationName;//仓库名

    public String getColorValue() {
        return colorValue;
    }

    public void setColorValue(String colorValue) {
        this.colorValue = colorValue;
    }

    public String getSizeValue() {
        return sizeValue;
    }

    public void setSizeValue(String sizeValue) {
        this.sizeValue = sizeValue;
    }

    public String getStyleValue() {
        return styleValue;
    }

    public void setStyleValue(String styleValue) {
        this.styleValue = styleValue;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(String goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public Integer getSpecId() {
        return specId;
    }

    public void setSpecId(Integer specId) {
        this.specId = specId;
    }

    public Long getCurrentQty() {
        return currentQty;
    }

    public void setCurrentQty(Long currentQty) {
        this.currentQty = currentQty;
    }
    //    public String getSpecName() {
//        return specName;
//    }
//
//    public void setSpecName(String specName) {
//        this.specName = specName;
//    }

    public String getSpecNumber() {
        return specNumber;
    }

    public void setSpecNumber(String specNumber) {
        this.specNumber = specNumber;
    }

}
