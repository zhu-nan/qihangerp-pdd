package com.b2c.entity.erp.vo;

import com.b2c.entity.ErpContactAddressEntity;

import java.util.List;

public class ErpContactAddressVo {
    private  Long id;

    //客户名称
    private  String name ;

    //联系方式
    private  String contact ;

    //省
    private  String province;

    //市
    private  String city;

    //区县
    private  String county;

    //收货地址详情
    private  String address;

    private List<ErpContactAddressEntity> addressList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<ErpContactAddressEntity> getAddressList() {
        return addressList;
    }

    public void setAddressList(List<ErpContactAddressEntity> addressList) {
        this.addressList = addressList;
    }
}
