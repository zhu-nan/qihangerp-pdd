package com.b2c.entity.erp.vo;

import com.b2c.entity.ErpGoodsBadStockEntity;

public class ErpGoodsBadStockVo extends ErpGoodsBadStockEntity {


    private String goodsName;            //商品名称
    private String goodsNumber;          //商品编码
    private String colorValue;//颜色
    private String sizeValue;//尺码

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(String goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public String getColorValue() {
        return colorValue;
    }

    public void setColorValue(String colorValue) {
        this.colorValue = colorValue;
    }

    public String getSizeValue() {
        return sizeValue;
    }

    public void setSizeValue(String sizeValue) {
        this.sizeValue = sizeValue;
    }
}

