package com.b2c.entity.shop;

import java.math.BigDecimal;

public class ShopTrafficEntity {
    private Long id;
     private Integer shopId;
     private Integer shopType;

    private Integer shows;
    private Integer visits;
    private Integer visitsFs;
    private Integer views;

    private Integer orderUsers;
    private Integer orderNewUsers;
    private Integer orders;
    private BigDecimal orderLKR;//订单老客占比
    private BigDecimal orderAmount;//GMV


    private Integer goods;
    private Integer goodsCount;//记录数据的商品数量
    private Integer collects;
    private Integer carts;

   
    private BigDecimal CVR;//成交转化率
    private BigDecimal CTR;//点击转化率
    private BigDecimal ATV;//客单价
    private BigDecimal UVV;//成交UV价值
    private BigDecimal SH;//整体商品搜索热度（拼多多）
    private BigDecimal SS;//整体搜索销售指数（拼多多）

    


    private BigDecimal ZBSC;//直播时长
    private BigDecimal ZBRJGK;
    private Integer ZBViews;
    private Integer ZBOrders;
    private BigDecimal ZBOrderAmount;
    private BigDecimal ZBCVR;


    private Integer FSNew;//新增关注
    private Integer FSOrders;
    private BigDecimal FSROrder;//粉丝订单占比
    private BigDecimal FSOrderAmount;
    private BigDecimal FSFGL;//粉丝复购率


    private Integer showsQZ;
    private Integer visitsQZ;
    private Integer ordersQZ;
    private BigDecimal roiQZ;
    private BigDecimal feeQZ;

    private Integer showsSS;
    private Integer visitsSS;
    private Integer ordersSS;
    private BigDecimal roiSS;
    private BigDecimal feeSS;
    private Integer collectsSS;
    
    private Integer showsCJ;
    private Integer visitsCJ;
    private Integer ordersCJ;
    private BigDecimal roiCJ;
    private BigDecimal feeCJ;
    private Integer collectsCJ;

    private BigDecimal dsr;
    
    private String date;
    private String remark;
    private String source;
    private String sourceName;
    private String createOn;
    private String modifyOn;

    
    
    public String getCreateOn() {
        return createOn;
    }
    public void setCreateOn(String createOn) {
        this.createOn = createOn;
    }
    public String getModifyOn() {
        return modifyOn;
    }
    public void setModifyOn(String modifyOn) {
        this.modifyOn = modifyOn;
    }
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Integer getShopId() {
        return shopId;
    }
    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }
    public Integer getShopType() {
        return shopType;
    }
    public void setShopType(Integer shopType) {
        this.shopType = shopType;
    }
    
    public Integer getShows() {
        return shows;
    }
    public void setShows(Integer shows) {
        this.shows = shows;
    }
    
    public Integer getVisits() {
        return visits;
    }
    public void setVisits(Integer visits) {
        this.visits = visits;
    }
   
    public Integer getViews() {
        return views;
    }
    public void setViews(Integer views) {
        this.views = views;
    }
    public Integer getOrders() {
        return orders;
    }
    public void setOrders(Integer orders) {
        this.orders = orders;
    }
    
    public Integer getCollects() {
        return collects;
    }
    public void setCollects(Integer collects) {
        this.collects = collects;
    }
   
    public Integer getCarts() {
        return carts;
    }
    public void setCarts(Integer carts) {
        this.carts = carts;
    }
    
    public Integer getOrderUsers() {
        return orderUsers;
    }
    public void setOrderUsers(Integer orderUsers) {
        this.orderUsers = orderUsers;
    }
    public Integer getOrderNewUsers() {
        return orderNewUsers;
    }
    public void setOrderNewUsers(Integer orderNewUsers) {
        this.orderNewUsers = orderNewUsers;
    }
   
    public BigDecimal getOrderAmount() {
        return orderAmount;
    }
    public void setOrderAmount(BigDecimal orderAmount) {
        this.orderAmount = orderAmount;
    }
    public BigDecimal getATV() {
        return ATV;
    }
    public void setATV(BigDecimal aTV) {
        ATV = aTV;
    }
    public BigDecimal getCVR() {
        return CVR;
    }
    public void setCVR(BigDecimal cVR) {
        CVR = cVR;
    }
    public BigDecimal getCTR() {
        return CTR;
    }
    public void setCTR(BigDecimal cTR) {
        CTR = cTR;
    }
    public BigDecimal getUVV() {
        return UVV;
    }
    public void setUVV(BigDecimal uVV) {
        UVV = uVV;
    }
    public BigDecimal getSH() {
        return SH;
    }
    public void setSH(BigDecimal sH) {
        SH = sH;
    }
    public BigDecimal getSS() {
        return SS;
    }
    public void setSS(BigDecimal sS) {
        SS = sS;
    }
    public Integer getVisitsFs() {
        return visitsFs;
    }
    public void setVisitsFs(Integer visitsFs) {
        this.visitsFs = visitsFs;
    }

    public String getSource() {
        return source;
    }
    public void setSource(String source) {
        this.source = source;
    }
    public String getSourceName() {
        return sourceName;
    }
    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String getRemark() {
        return remark;
    }
    public void setRemark(String remark) {
        this.remark = remark;
    }
    public Integer getGoods() {
        return goods;
    }
    public void setGoods(Integer goods) {
        this.goods = goods;
    }
    public BigDecimal getOrderLKR() {
        return orderLKR;
    }
    public void setOrderLKR(BigDecimal orderLKR) {
        this.orderLKR = orderLKR;
    }
    public BigDecimal getZBSC() {
        return ZBSC;
    }
    public void setZBSC(BigDecimal zBSC) {
        ZBSC = zBSC;
    }
    public BigDecimal getZBRJGK() {
        return ZBRJGK;
    }
    public void setZBRJGK(BigDecimal zBRJGK) {
        ZBRJGK = zBRJGK;
    }
    public Integer getZBViews() {
        return ZBViews;
    }
    public void setZBViews(Integer zBViews) {
        ZBViews = zBViews;
    }
    public Integer getZBOrders() {
        return ZBOrders;
    }
    public void setZBOrders(Integer zBOrders) {
        ZBOrders = zBOrders;
    }
    public BigDecimal getZBOrderAmount() {
        return ZBOrderAmount;
    }
    public void setZBOrderAmount(BigDecimal zBOrderAmount) {
        ZBOrderAmount = zBOrderAmount;
    }
    public BigDecimal getZBCVR() {
        return ZBCVR;
    }
    public void setZBCVR(BigDecimal zBCVR) {
        ZBCVR = zBCVR;
    }
    public Integer getFSNew() {
        return FSNew;
    }
    public void setFSNew(Integer fSNew) {
        FSNew = fSNew;
    }
    public Integer getFSOrders() {
        return FSOrders;
    }
    public void setFSOrders(Integer fSOrders) {
        FSOrders = fSOrders;
    }
    public BigDecimal getFSROrder() {
        return FSROrder;
    }
    public void setFSROrder(BigDecimal fSROrder) {
        FSROrder = fSROrder;
    }
    public BigDecimal getFSOrderAmount() {
        return FSOrderAmount;
    }
    public void setFSOrderAmount(BigDecimal fSOrderAmount) {
        FSOrderAmount = fSOrderAmount;
    }
    public BigDecimal getFSFGL() {
        return FSFGL;
    }
    public void setFSFGL(BigDecimal fSFGL) {
        FSFGL = fSFGL;
    }
    public Integer getShowsQZ() {
        return showsQZ;
    }
    public void setShowsQZ(Integer showsQZ) {
        this.showsQZ = showsQZ;
    }
    public Integer getVisitsQZ() {
        return visitsQZ;
    }
    public void setVisitsQZ(Integer visitsQZ) {
        this.visitsQZ = visitsQZ;
    }
    public Integer getOrdersQZ() {
        return ordersQZ;
    }
    public void setOrdersQZ(Integer ordersQZ) {
        this.ordersQZ = ordersQZ;
    }
    public BigDecimal getRoiQZ() {
        return roiQZ;
    }
    public void setRoiQZ(BigDecimal roiQZ) {
        this.roiQZ = roiQZ;
    }
    public BigDecimal getFeeQZ() {
        return feeQZ;
    }
    public void setFeeQZ(BigDecimal feeQZ) {
        this.feeQZ = feeQZ;
    }
    public Integer getShowsSS() {
        return showsSS;
    }
    public void setShowsSS(Integer showsSS) {
        this.showsSS = showsSS;
    }
    public Integer getVisitsSS() {
        return visitsSS;
    }
    public void setVisitsSS(Integer visitsSS) {
        this.visitsSS = visitsSS;
    }
    public Integer getOrdersSS() {
        return ordersSS;
    }
    public void setOrdersSS(Integer ordersSS) {
        this.ordersSS = ordersSS;
    }
    public BigDecimal getRoiSS() {
        return roiSS;
    }
    public void setRoiSS(BigDecimal roiSS) {
        this.roiSS = roiSS;
    }
    public BigDecimal getFeeSS() {
        return feeSS;
    }
    public void setFeeSS(BigDecimal feeSS) {
        this.feeSS = feeSS;
    }
    public Integer getCollectsSS() {
        return collectsSS;
    }
    public void setCollectsSS(Integer collectsSS) {
        this.collectsSS = collectsSS;
    }
    public Integer getShowsCJ() {
        return showsCJ;
    }
    public void setShowsCJ(Integer showsCJ) {
        this.showsCJ = showsCJ;
    }
    public Integer getVisitsCJ() {
        return visitsCJ;
    }
    public void setVisitsCJ(Integer visitsCJ) {
        this.visitsCJ = visitsCJ;
    }
    public Integer getOrdersCJ() {
        return ordersCJ;
    }
    public void setOrdersCJ(Integer ordersCJ) {
        this.ordersCJ = ordersCJ;
    }
    public BigDecimal getRoiCJ() {
        return roiCJ;
    }
    public void setRoiCJ(BigDecimal roiCJ) {
        this.roiCJ = roiCJ;
    }
    public BigDecimal getFeeCJ() {
        return feeCJ;
    }
    public void setFeeCJ(BigDecimal feeCJ) {
        this.feeCJ = feeCJ;
    }
    public Integer getCollectsCJ() {
        return collectsCJ;
    }
    public void setCollectsCJ(Integer collectsCJ) {
        this.collectsCJ = collectsCJ;
    }
    public Integer getGoodsCount() {
        return goodsCount;
    }
    public void setGoodsCount(Integer goodsCount) {
        this.goodsCount = goodsCount;
    }
    public BigDecimal getDsr() {
        return dsr;
    }
    public void setDsr(BigDecimal dsr) {
        this.dsr = dsr;
    }
    

}
