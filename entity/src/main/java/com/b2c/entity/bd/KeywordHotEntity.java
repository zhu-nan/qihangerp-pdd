package com.b2c.entity.bd;

import java.math.BigDecimal;

public class KeywordHotEntity {
    private Long id;
    private Long parentId;
    private String category;
   
    private String platform;
    private String keyword;
    private Integer ranking;
    private Integer goodsCount;
    private Integer sousuorenqi;
    private Integer sousuoredu;
    private Integer dianjirenqi;
    private Integer dianjiredu;
    private BigDecimal dianjilv;
    private BigDecimal zhifulv;
    private Float jingzhengzhishu;
    private Float chengjiaozhishu;

    private Integer isDelete;
    private String includeDate;
    private String createTime;
    private String modifyTime;



    public Integer getSousuoredu() {
        return sousuoredu;
    }

    public void setSousuoredu(Integer sousuoredu) {
        this.sousuoredu = sousuoredu;
    }

    public Integer getDianjiredu() {
        return dianjiredu;
    }

    public void setDianjiredu(Integer dianjiredu) {
        this.dianjiredu = dianjiredu;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Integer getSousuorenqi() {
        return sousuorenqi;
    }

    public void setSousuorenqi(Integer sousuorenqi) {
        this.sousuorenqi = sousuorenqi;
    }

    public Integer getDianjirenqi() {
        return dianjirenqi;
    }

    public void setDianjirenqi(Integer dianjirenqi) {
        this.dianjirenqi = dianjirenqi;
    }

    public BigDecimal getDianjilv() {
        return dianjilv;
    }

    public void setDianjilv(BigDecimal dianjilv) {
        this.dianjilv = dianjilv;
    }

    public BigDecimal getZhifulv() {
        return zhifulv;
    }

    public void setZhifulv(BigDecimal zhifulv) {
        this.zhifulv = zhifulv;
    }


    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public Integer getRanking() {
        return ranking;
    }

    public void setRanking(Integer ranking) {
        this.ranking = ranking;
    }

    public Integer getGoodsCount() {
        return goodsCount;
    }

    public void setGoodsCount(Integer goodsCount) {
        this.goodsCount = goodsCount;
    }

    public Float getJingzhengzhishu() {
        return jingzhengzhishu;
    }

    public void setJingzhengzhishu(Float jingzhengzhishu) {
        this.jingzhengzhishu = jingzhengzhishu;
    }

    public Float getChengjiaozhishu() {
        return chengjiaozhishu;
    }

    public void setChengjiaozhishu(Float chengjiaozhishu) {
        this.chengjiaozhishu = chengjiaozhishu;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getIncludeDate() {
        return includeDate;
    }

    public void setIncludeDate(String includeDate) {
        this.includeDate = includeDate;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}
