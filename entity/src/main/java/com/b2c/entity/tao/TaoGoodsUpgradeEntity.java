package com.b2c.entity.tao;

public class TaoGoodsUpgradeEntity {
    private Integer id;
    private Long goodsId;
    private String oldTitle;
    private Integer oldSales;
    private String newTitle;
    private String remark;
    private String date;
    private String createTime;
    private String result;
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getOldTitle() {
        return oldTitle;
    }
    public void setOldTitle(String oldTitle) {
        this.oldTitle = oldTitle;
    }
    public Integer getOldSales() {
        return oldSales;
    }
    public void setOldSales(Integer oldSales) {
        this.oldSales = oldSales;
    }
    public String getNewTitle() {
        return newTitle;
    }
    public void setNewTitle(String newTitle) {
        this.newTitle = newTitle;
    }
    public String getRemark() {
        return remark;
    }
    public void setRemark(String remark) {
        this.remark = remark;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String getCreateTime() {
        return createTime;
    }
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
    public Long getGoodsId() {
        return goodsId;
    }
    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }
    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    
}
