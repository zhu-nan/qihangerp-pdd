package com.b2c.entity;

import java.util.List;

/**
 * 描述：
 *
 * @author qlp
 * @date 2019-10-15 15:00
 */
public class ErpOrderReturnEntity {
    private Long id;//` bigint(20) NOT NULL AUTO_INCREMENT,
    private String orderNum;//` varchar(50) NOT NULL DEFAULT '' COMMENT '退货单号',
    private String sourceOrderNum;//源订单号
    private Long orderTime;//` bigint(20) NOT NULL COMMENT '退货单下单时间',
    private Long createTime;//` bigint(20) NOT NULL COMMENT '创建时间',
    private Long modifyTime;//` bigint(20) DEFAULT NULL COMMENT '修改时间',
    private Long receiveTime;//` bigint(20) DEFAULT NULL COMMENT '发货时间',
    private String logisticsCompany;//` varchar(30) DEFAULT NULL COMMENT '物流公司',
    private String logisticsCompanyCode;//` varchar(30) DEFAULT NULL COMMENT '物流公司编码',
    private String logisticsCode;//` varchar(30) DEFAULT NULL COMMENT '物流单号',
    private String remark;//` varchar(50) DEFAULT '' COMMENT '备注',
    private String contactPerson;// varchar(20) NOT NULL DEFAULT '' COMMENT '发货人',
    private String mobile;//` varchar(20) NOT NULL DEFAULT '' COMMENT '发货人手机号',
    private String address;//` varchar(50) NOT NULL DEFAULT '' COMMENT '发货地址',
    private String source;//` varchar(10) NOT NULL DEFAULT '' COMMENT '订单来源（ALIBABA，TAOBAO，YOUZAN，YUNGOU，ERP，OFFLINE）',
    private Integer status;//` int(11) NOT NULL COMMENT '状态（0未处理1确认收货）',
    private Long totalQuantity;//总商品数量
    private String shopName;//店铺名
    private Integer shopId;//店铺id

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getSourceOrderNum() {
        return sourceOrderNum;
    }

    public void setSourceOrderNum(String sourceOrderNum) {
        this.sourceOrderNum = sourceOrderNum;
    }

    public Long getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Long totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public Long getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Long orderTime) {
        this.orderTime = orderTime;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Long modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Long getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(Long receiveTime) {
        this.receiveTime = receiveTime;
    }

    public String getLogisticsCompany() {
        return logisticsCompany;
    }

    public void setLogisticsCompany(String logisticsCompany) {
        this.logisticsCompany = logisticsCompany;
    }

    public String getLogisticsCompanyCode() {
        return logisticsCompanyCode;
    }

    public void setLogisticsCompanyCode(String logisticsCompanyCode) {
        this.logisticsCompanyCode = logisticsCompanyCode;
    }

    public String getLogisticsCode() {
        return logisticsCode;
    }

    public void setLogisticsCode(String logisticsCode) {
        this.logisticsCode = logisticsCode;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
